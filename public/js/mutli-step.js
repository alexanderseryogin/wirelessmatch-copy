
$(document).ready(function(){
    let sendForm = false;
    let button = null;

    $("#phone").intlTelInput({
        initialCountry: "us",
        responsiveDropdown: true,
        separateDialCode: true,
        utilsScript: "/build/js/utils.js",
        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                let countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        }
    });

    $("#unlimited-data").val($('.btn-data.active').text());

    function nextStep(){
        let nextStep = $(button).parent().next();
        $(button).parent().hide();
        nextStep.show();
        nextStep.find(".required").attr("required", "true");
    }

    $('.next').click(function () {
        button = this;
        let lines = $('#select-line option:selected').text();
        $('#question-step-3').html('What are your ' + lines + ' devices?');

        let carrier = $('#select-carrier option:selected').text();
        $('#input-tenure').attr('placeholder', 'How long have you been with ' + carrier + '?')
    });
    $('#send-form').click(function () { sendForm = true; });

    $('#multi-step-form').submit(function(e){

        if(!$("#phone").intlTelInput("isValidNumber")){
            let error = 'The phone number is not valid.';
            $('.help-error').html('<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>');
            return false;
        }

        $('#phone-number').val($("#phone").intlTelInput("getNumber"));

        $.ajax({
            url: "/lead-check",
            method: "POST",
            async: false,
            data: $(this).serialize(),
            dataType: "html"
        }).done(function() {
            $('.help-error').html('');
            if(!sendForm){
                nextStep();
                e.preventDefault();
            }
        }).fail(function({responseText}) {
            let data = JSON.parse(responseText);
            let html = '';
            $.each(data.errors, function (index, error) {
                html += '<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>'
            });
            $('.help-error').html(html);
            e.preventDefault();
        });
    });

    $('.btn-data').click(function () {
        if($(this).hasClass('active'))
            return false;
        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
        $("#unlimited-data").val($(this).text());
    });

    $('.btn-plus').click(function () {
        let input = $(this).parent().children('input');
        let value = input.val();
        value = parseInt(value) + 1;
        if(value < 100){
            input.val(value);
        }
    });

    $('.btn-minus').click(function () {
        let input = $(this).parent().children('input');
        let value = input.val();
        value = parseInt(value) - 1;
        if(value >= 0){
            input.val(value);
        }
    });

    $('#phone').keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;
        $text = $(this);
        if (key !== 8 && key !== 9) {
            if ($text.val().length === 3) {
                $text.val($text.val() + '-');
            }
            if ($text.val().length === 7) {
                $text.val($text.val() + '-');
            }

        }

        return (key == 8 || key == 9 || key == 37 || key == 39   || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 116 );
    })

});
