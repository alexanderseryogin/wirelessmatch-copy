
$(document).ready(function(){

    localStorage.clear(); // I save the annual savings in localStorage(). So every time when refresh I clear it.

    let sendForm = false;
    let button = null;

    let priceLine = {
      1: 60,
      2: 100,
      3: 110,
      4: 120,
      5: 130,
      6: 140,
      7: 150,
      8: 160,
      9: 170,
      10: 180
    };


    //calculating savings
    let price_line_1 = 60;
    let price_line_2 = 100;
    let price_line_3 = 110;
    let price_line_4 = 120;
    let price_line_5 = 130;
    let price_line_6 = 140;
    let price_line_7 = 150;
    let price_line_8 = 160;
    let price_line_9 = 170;
    let price_line_10 = 180;


    // $("#phone").intlTelInput({
    //     initialCountry: "us",
    //     responsiveDropdown: true,
    //     separateDialCode: true,
    //     utilsScript: "/build/js/utils.js",
    //     geoIpLookup: function(callback) {
    //         $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //             let countryCode = (resp && resp.country) ? resp.country : "";
    //             callback(countryCode);
    //         });
    //     }
    // });
    //
    // $("#phone-2").intlTelInput({
    //     initialCountry: "us",
    //     responsiveDropdown: true,
    //     separateDialCode: true,
    //     utilsScript: "/build/js/utils.js",
    //     geoIpLookup: function(callback) {
    //         $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //             let countryCode = (resp && resp.country) ? resp.country : "";
    //             callback(countryCode);
    //         });
    //     }
    // });

    // $("#unlimited-data").val($('.btn-data.active').text());

    function nextStep(){
        let nextStep = $(button).parent().next();
        $(button).parent().hide();
        nextStep.show();
        nextStep.find(".required").attr("required", "true");
    }

    $('.next').click(function () {
        button = this;
        let lines = $('#select-line option:selected').text();
        $('#question-step-3').html('What are your ' + lines + ' devices?');

        let carrier = $('#select-carrier option:selected').text();
        $('#input-tenure').attr('placeholder', 'How long have you been with ' + carrier + '?')
    });
    $('.next-2').click(function () {
        button = this;
        let lines = $('#select-line-2 option:selected').text();
        $('#question-step-3').html('What are your ' + lines + ' devices?');

        let carrier = $('#select-carrier-2 option:selected').text();
        $('#input-tenure').attr('placeholder', 'How long have you been with ' + carrier + '?')
    });

    $('#send-form').click(function () { sendForm = true; });
    $('#send-form-2').click(function () { sendForm = true; });

    $('#multi-step-form').submit(function(e){

        if(!$("#phone").intlTelInput("isValidNumber")){
            let error = 'The phone number is not valid.';
            $('.help-error').html('<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>');
            return false;
        }

        $('#phone-number').val($("#phone").intlTelInput("getNumber"));

        $.ajax({
            url: "/lead-check",
            method: "POST",
            async: false,
            data: $(this).serialize(),
            dataType: "html"
        }).done(function() {
            $('.help-error').html('');
            // if(!sendForm){
            //     nextStep();
            //     e.preventDefault();
            // }
        }).fail(function({responseText}) {
            let data = JSON.parse(responseText);
            let html = '';
            $.each(data.errors, function (index, error) {
                html += '<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>'
            });
            $('.help-error').html(html);
            e.preventDefault();
        });
    });

    $('form#multi-step-form-2 button#send-form-2').parent().css('display', 'none');
    $('form#multi-step-form-2').submit(function(e){

        if(!$("input#phone-2").intlTelInput("isValidNumber")){
            let error = 'The phone number is not valid.';
            $('.help-error').html('<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>');
            return false;
        }

        $('input#phone-number-2').val($("input#phone-2").intlTelInput("getNumber"));

        $.ajax({
            url: "/lead-check",
            method: "POST",
            async: false,
            data: $(this).serialize(),
            dataType: "html"
        }).done(function() {
            $('.help-error').html('');
            // if(!sendForm){
            //     nextStep();
            //     e.preventDefault();
            // }
        }).fail(function({responseText}) {
            let data = JSON.parse(responseText);
            let html = '';
            $.each(data.errors, function (index, error) {
                html += '<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>'
            });
            $('.help-error').html(html);
            e.preventDefault();
        });
    });

    // $('.btn-data').click(function () {
    //     if($(this).hasClass('active'))
    //         return false;
    //     $(this).parent().find('.active').removeClass('active');
    //     $(this).addClass('active');
    //     $("#unlimited-data").val($(this).text());
    // });
    //
    // $('.btn-plus').click(function () {
    //     let input = $(this).parent().children('input');
    //     let value = input.val();
    //     value = parseInt(value) + 1;
    //     if(value < 100){
    //         input.val(value);
    //     }
    // });
    //
    // $('.btn-minus').click(function () {
    //     let input = $(this).parent().children('input');
    //     let value = input.val();
    //     value = parseInt(value) - 1;
    //     if(value >= 0){
    //         input.val(value);
    //     }
    // });
    //
    // $('#phone').keydown(function (e) {
    //     var key = e.charCode || e.keyCode || 0;
    //     $text = $(this);
    //     if (key !== 8 && key !== 9) {
    //         if ($text.val().length === 3) {
    //             $text.val($text.val() + '-');
    //         }
    //         if ($text.val().length === 7) {
    //             $text.val($text.val() + '-');
    //         }
    //
    //     }
    //
    //     return (key == 8 || key == 9 || key == 37 || key == 39   || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 116 );
    // })
    //
    // $('#phone-2').keydown(function (e) {
    //     var key = e.charCode || e.keyCode || 0;
    //     $text = $(this);
    //     if (key !== 8 && key !== 9) {
    //         if ($text.val().length === 3) {
    //             $text.val($text.val() + '-');
    //         }
    //         if ($text.val().length === 7) {
    //             $text.val($text.val() + '-');
    //         }
    //
    //     }
    //
    //     return (key == 8 || key == 9 || key == 37 || key == 39   || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 116 );
    // });

    // automatically add '$' sign to 4th input
    // $('input#current_bill').keyup(function() {
    //     $(this).val(function(i,v) {
    //         return '$' + v.replace('$',''); //remove exisiting, add back.
    //     });
    // });

    // automatically add '$' sign to 4th input
    // $('input#current_bill_2').keyup(function() {
    //     $(this).val(function(i,v) {
    //         return '$' + v.replace('$',''); //remove exisiting, add back.
    //     });
    // });

    function changeBtnText(text){
        return $('button[type=submit]').html(text);
    }


    function output(lines) {
        let clientPays = $('input#current_bill').val();

        function annualSaving(clientsPays, lines){
            return (clientsPays-price_line)*12
        }
        function annualSavingsPercentage(clientsPays, price_line){
            let coef = (clientsPays-price_line)/clientsPays;
            let percents = coef * 100;
            let percentYear = percents * 12;
            return percents;
        }
        // if (priceLine[lines] === undefined){
        //     console.log('Error_1'); return;
        // } else {
        //
        // }
        let price_line = priceLine[lines];
        localStorage.setItem('annualSavings', annualSaving(clientPays, price_line));
        localStorage.setItem('annualSavingsPercentage', annualSavingsPercentage(clientPays, price_line));
        $('.response').html('Your annual saving is ' + annualSaving(clientPays, price_line));
        $('.response-percent').html('Your annual saving is ' + annualSavingsPercentage(clientPays, price_line) + '%');
        changeBtnText('Save ' + annualSaving(clientPays, price_line) + '$ Now!');
    }


    $('input#current_bill, select#select-line').on('change', function () {
        let clientPays = $('input#current_bill').val();
        let lines = parseInt($('select#select-line').val());
        output(lines);

    });

    console.log('public/js/script-test.js');
});
