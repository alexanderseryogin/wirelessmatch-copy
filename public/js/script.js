
$(document).ready(function(){
    let sendForm = false;
    let button = null;

    //calculating savings
    let line_1 = 60;
    let line_2 = 100;
    let line_3 = 120;
    let line_4 = 120;
    let line_5 = 120;
    let line_6 = 120;
    let line_7 = 120;
    let line_8 = 120;
    let line_9 = 120;
    let line_10 = 120;


    // $("#phone").intlTelInput({
    //     initialCountry: "us",
    //     responsiveDropdown: true,
    //     separateDialCode: true,
    //     utilsScript: "/build/js/utils.js",
    //     geoIpLookup: function(callback) {
    //         $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //             let countryCode = (resp && resp.country) ? resp.country : "";
    //             callback(countryCode);
    //         });
    //     }
    // });
    //
    // $("#phone-2").intlTelInput({
    //     initialCountry: "us",
    //     responsiveDropdown: true,
    //     separateDialCode: true,
    //     utilsScript: "/build/js/utils.js",
    //     geoIpLookup: function(callback) {
    //         $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //             let countryCode = (resp && resp.country) ? resp.country : "";
    //             callback(countryCode);
    //         });
    //     }
    // });

    // $("#unlimited-data").val($('.btn-data.active').text());

    function nextStep(){
        let nextStep = $(button).parent().next();
        $(button).parent().hide();
        nextStep.show();
        nextStep.find(".required").attr("required", "true");
    }

    $('.next').click(function () {
        button = this;
        let lines = $('#select-line option:selected').text();
        $('#question-step-3').html('What are your ' + lines + ' devices?');

        let carrier = $('#select-carrier option:selected').text();
        $('#input-tenure').attr('placeholder', 'How long have you been with ' + carrier + '?')
    });
    $('.next-2').click(function () {
        button = this;
        let lines = $('#select-line-2 option:selected').text();
        $('#question-step-3').html('What are your ' + lines + ' devices?');

        let carrier = $('#select-carrier-2 option:selected').text();
        $('#input-tenure').attr('placeholder', 'How long have you been with ' + carrier + '?')
    });


    $('#send-form').click(function () { sendForm = true; });
    $('#send-form-2').click(function () { sendForm = true; });

    $('#multi-step-form').submit(function(e){

        if(!$("#phone").intlTelInput("isValidNumber")){
            let error = 'The phone number is not valid.';
            $('.help-error').html('<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>');
            return false;
        }

        $('#phone-number').val($("#phone").intlTelInput("getNumber"));

        $.ajax({
            url: "/lead-check",
            method: "POST",
            async: false,
            data: $(this).serialize(),
            dataType: "html"
        }).done(function() {
            $('.help-error').html('');
            // if(!sendForm){
            //     nextStep();
            //     e.preventDefault();
            // }
        }).fail(function({responseText}) {
            let data = JSON.parse(responseText);
            let html = '';
            $.each(data.errors, function (index, error) {
                html += '<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>'
            });
            $('.help-error').html(html);
            e.preventDefault();
        });
    });

    $('form#multi-step-form-2 button#send-form-2').parent().css('display', 'none');
    $('form#multi-step-form-2').submit(function(e){

        if(!$("input#phone-2").intlTelInput("isValidNumber")){
            let error = 'The phone number is not valid.';
            $('.help-error').html('<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>');
            return false;
        }

        $('input#phone-number-2').val($("input#phone-2").intlTelInput("getNumber"));

        $.ajax({
            url: "/lead-check",
            method: "POST",
            async: false,
            data: $(this).serialize(),
            dataType: "html"
        }).done(function() {
            $('.help-error').html('');
            // if(!sendForm){
            //     nextStep();
            //     e.preventDefault();
            // }
        }).fail(function({responseText}) {
            let data = JSON.parse(responseText);
            let html = '';
            $.each(data.errors, function (index, error) {
                html += '<div><i class="fa fa-exclamation-triangle"></i> '+ error +'</div>'
            });
            $('.help-error').html(html);
            e.preventDefault();
        });
    });

    // $('.btn-data').click(function () {
    //     if($(this).hasClass('active'))
    //         return false;
    //     $(this).parent().find('.active').removeClass('active');
    //     $(this).addClass('active');
    //     $("#unlimited-data").val($(this).text());
    // });
    //
    // $('.btn-plus').click(function () {
    //     let input = $(this).parent().children('input');
    //     let value = input.val();
    //     value = parseInt(value) + 1;
    //     if(value < 100){
    //         input.val(value);
    //     }
    // });
    //
    // $('.btn-minus').click(function () {
    //     let input = $(this).parent().children('input');
    //     let value = input.val();
    //     value = parseInt(value) - 1;
    //     if(value >= 0){
    //         input.val(value);
    //     }
    // });
    //
    // $('#phone').keydown(function (e) {
    //     var key = e.charCode || e.keyCode || 0;
    //     $text = $(this);
    //     if (key !== 8 && key !== 9) {
    //         if ($text.val().length === 3) {
    //             $text.val($text.val() + '-');
    //         }
    //         if ($text.val().length === 7) {
    //             $text.val($text.val() + '-');
    //         }
    //
    //     }
    //
    //     return (key == 8 || key == 9 || key == 37 || key == 39   || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 116 );
    // })
    //
    // $('#phone-2').keydown(function (e) {
    //     var key = e.charCode || e.keyCode || 0;
    //     $text = $(this);
    //     if (key !== 8 && key !== 9) {
    //         if ($text.val().length === 3) {
    //             $text.val($text.val() + '-');
    //         }
    //         if ($text.val().length === 7) {
    //             $text.val($text.val() + '-');
    //         }
    //
    //     }
    //
    //     return (key == 8 || key == 9 || key == 37 || key == 39   || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 116 );
    // });

    // automatically add '$' sign to 4th input
    // $('input#current_bill').keyup(function() {
    //     $(this).val(function(i,v) {
    //         return '$' + v.replace('$',''); //remove exisiting, add back.
    //     });
    // });

    // automatically add '$' sign to 4th input
    // $('input#current_bill_2').keyup(function() {
    //     $(this).val(function(i,v) {
    //         return '$' + v.replace('$',''); //remove exisiting, add back.
    //     });
    // });

    function changeBtnText(text){
        return $('button[type=submit]').html(text);
    }

    $('select#select-line').on('change', function () {

        function annualSaving(clientsPays, line){
            return (clientsPays-line)*12
        }

        let lines = parseInt($(this).val());
        console.log('lines:'+lines);
        let clientPays = $('input#current_bill').val();
        console.log('Client pays ' + clientPays);

        switch (lines) {
            case 1:
                console.log('Our price '+line_1);
                console.log('Your annual savings is ' + (clientPays-line_1) * 12);
                $('.response').html('Your annual saving is ' + annualSaving(clientPays, line_1));
                changeBtnText('Save ' + annualSaving(clientPays, line_1) + '$ Now!');

                break;
            case 2:
                console.log('Our price '+line_2);
                console.log('Your annual savings is ' + (clientPays-line_2) * 12);
                $('.response').html('Your annual saving is ' + annualSaving(clientPays, line_2));
                changeBtnText('Save ' + annualSaving(clientPays, line_2) + '$ Now!');
                break;
            case 3:
                console.log('Our price '+line_3);
                console.log('Your annual savings is ' + (clientPays-line_3) * 12);
                $('.response').html('Your annual saving is ' + annualSaving(clientPays, line_3));
                changeBtnText('Save ' + annualSaving(clientPays, line_3) + '$ Now!');
                break;
            case 4:
                console.log('Our price '+line_4);
                break;
            case 5:
                console.log('Our price '+line_5);
                break;
            case 6:
                console.log('Our price '+line_6);
                break;
            case 7:
                console.log('Our price '+line_7);
                break;
            case 8:
                console.log('Our price '+line_8);
                break;
            case 9:
                console.log('Our price '+line_9);
                break;
            case 10:
                console.log('Our price '+line_10);
                break;
            default:
                console.log('Error_1');
                break;
        }

    });


    $('input#current_bill').on('change', function () {

        console.log('current_bill changed to ' + $(this).val());

        function annualSaving(clientsPays, line){
            return (clientsPays-line)*12
        }

        let lines = parseInt($('select#select-line').val());
        console.log('Lines: '+lines);

        let clientPays = parseInt($('input#current_bill').val());
        console.log('Client pays ' + clientPays);

        switch (lines) {
            case 1:
                console.log('Our price '+line_1);
                console.log('Client pays '+clientPays);
                console.log('Your annual savings is ' + annualSaving(clientPays, line_1));
                $('.response').html('Your annual saving is ' + annualSaving(clientPays, line_1));
                changeBtnText('Save ' + annualSaving(clientPays, line_1) + '$ Now!');

                break;
            case 2:
                console.log('Our price '+line_2);
                console.log('Client pays '+clientPays);
                console.log('Your annual savings is ' + annualSaving(clientPays, line_2));
                $('.response').html('Your annual saving is ' + annualSaving(clientPays, line_2));
                changeBtnText('Save ' + annualSaving(clientPays, line_2) + '$ Now!');
                break;
            case 3:
                console.log('Our price '+line_3);
                console.log('Client pays '+clientPays);
                console.log('Your annual savings is ' + annualSaving(clientPays, line_3));
                $('.response').html('Your annual saving is ' + annualSaving(clientPays, line_3));
                changeBtnText('Save ' + annualSaving(clientPays, line_3) + '$ Now!');
                break;
            case 4:
                console.log('Our price '+line_4);
                break;
            case 5:
                console.log('Our price '+line_5);
                break;
            case 6:
                console.log('Our price '+line_6);
                break;
            case 7:
                console.log('Our price '+line_7);
                break;
            case 8:
                console.log('Our price '+line_8);
                break;
            case 9:
                console.log('Our price '+line_9);
                break;
            case 10:
                console.log('Our price '+line_10);
                break;
            default:
                console.log('Error_2');
                break;
        }

    });


    console.log('public/js/script.js');
});
