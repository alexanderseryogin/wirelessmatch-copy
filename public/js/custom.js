$(function(){
    //work-slider
    $('.work-main').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: false,
      centerMode: true,
      arrows: false,
      infinite: true,
      speed: 1500,
      autoplaySpeed: 2500,
      responsive: [
      {
        breakpoint: 1024,
        settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '.w-left',
        nextArrow: '.w-right',
        centerPadding: '0px',
        }
      },
      {
        breakpoint: 481,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '.w-left',
        nextArrow: '.w-right',
        centerPadding: '0px',
        }
      }
    ]
    });    
    
    //client-main-slider
    $('.client-main').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: false,
      arrows: true,
      prevArrow: '.c-left',
      nextArrow: '.c-right',
      infinite: true,
      speed: 1500,
      autoplaySpeed: 2500,
      responsive: [
      {
        breakpoint: 1024,
        settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        }
      },
      {
        breakpoint: 481,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '0px',
        }
      }
    ]
    });
    
    //$('.venobox').venobox();
    
    
    
    
    
});