let map;
let isValidAddress = false;

function initialize() {
    initMap();
}

let latitude = parseFloat($('#latitude').val());
let longitude =  parseFloat($('#longitude').val());

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: latitude, lng: longitude},
        zoom: 9
    });

    let input = document.getElementById('pac-input');
    let autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

    let infowindow = new google.maps.InfoWindow();
    let infowindowContent = document.getElementById('infowindow-content');

    infowindow.setContent(infowindowContent);
    let marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        let place = autocomplete.getPlace();
        if (!place.geometry) {
            isValidAddress = false;
            if(!input.hasAttribute('required')){
                window.alert("No details available for input: '" + input.value + "'");
                input.value = '';
            }
            return;
        }

        isValidAddress = true;

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        let address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);
    });

    $('#multi-step-form').submit(function(e){
        if(!isValidAddress && input.value !== '' && input.hasAttribute('required')) {
            window.alert("No details available for input: '" + input.value + "'");
            input.value = '';
            e.preventDefault();
        }
    });

    $('#pac-input').keydown(function(event){
        if(event.keyCode === 13  && !input.hasAttribute('required')){
            event.preventDefault();
            return false;
        }
    });
}