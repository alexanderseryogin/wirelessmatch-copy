$(function () { // begin jQuery

    console.log('admin.js OK');

    $('button').click(function () {
       let buttonId = $(this).data('button_id');
       let buttonText = $('.button-id-'+buttonId).val();
       console.log('Button text: ' + buttonText);

       $.ajax({
           url: '/admin/dashboard/change-button-text',
           data: {
               button_id: buttonId,
               button_text: buttonText
           },
           success: function (response) {
               console.log(response);
               window.location.reload();
           },
           error: function (response) {
               console.log(response.error())
           }
       })
    });


}); //end jQuery