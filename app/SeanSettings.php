<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeanSettings extends Model
{
    // Sean, here you can change the settings as you wish.
    // string have to be written within ''. It's a must.
    // numbers can be written without ''.

    //script-test.*
    const SCRIPT_TEST__TOP_TITLE_1 = 'CUT YOUR CELL';
    const SCRIPT_TEST__TOP_TITLE_2 = 'PHONE BILL IN HALF.';
    const SCRIPT_TEST__FORM_TITLE = 'Just 2 questions...!';
    const SCRIPT_TEST__FORM_TOP__PLACEHOLDER_1 = 'Current bill?';
    const SCRIPT_TEST__FORM_TOP__PLACEHOLDER_2 = 'How many lines?';
    const SCRIPT_TEST__FORM_BUTTON_TEXT = 'Calculate Savings';
}
