<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class RemoveAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all()->pluck('email')->toArray();

        if(count($users)){
            $email = $this->choice('Choice admin email', $users);
            if ($this->confirm('Are you sure you want to delete admin "' . $email . '"?')) {
                User::query()->where('email', $email)->delete();
                $this->info('Success! Admin "' . $email . '" removed.');
            }
        }else{
            $this->error('Error! Admins not found.');
        }

    }
}
