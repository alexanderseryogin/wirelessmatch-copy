<?php

namespace App\Http\Requests;

use App\Rules\CustomName;
use Illuminate\Foundation\Http\FormRequest;

class LeadCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if ($this->attributes->has('name')) {
            $rules['name'] = '\'min:4\', \'max:255\', \'required\', \'string\', new CustomName()';
        }

        if ($this->attributes->has('phone')) {
            $rules['phone'] = '\'phone\' => \'required\'';
        }

//        $rules = [
//            'name' => ['min:4', 'max:255', 'required', 'string', new CustomName()],
//            'phone' => 'required'
//        ];

        if($this->zip_code)
            $rules['zip_code'] = 'numeric|digits_between:5,8';

        return $rules;
    }

}
