<?php

namespace App\Http\Controllers;

use App\Dashboard\AdminButton;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function index2()
    {
        return view('admin.dashboard.index2');
    }

    public function index3()
    {
        return view('admin.dashboard.index3');
    }

    public function index4()
    {
        $admin_buttons = DB::table('admin_buttons')->get();
        $admin_buttons = AdminButton::all()->where('page_id', '=', 4)->keyBy('id')->toArray();
        return view('admin.dashboard.index4', [
            'admin_buttons' => $admin_buttons
        ]);
    }

    public function changeButtonText()
    {
//        $button_text = Input::get('button_text');
        $button_id = Input::get('button_id', '');
        $button_text = Input::get('button_text', '');
        try{
            DB::table('admin_buttons')
                ->where('id', $button_id)
                ->update(['text' => $button_text]);
        } catch (\Exception $exception){

        }

    }
}
