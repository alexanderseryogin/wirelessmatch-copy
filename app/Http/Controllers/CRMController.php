<?php

namespace App\Http\Controllers;

use App\Dashboard\AdminButton;
use App\Http\Requests\LeadCreateRequest;
use App\SeanSettings;
use App\Services\ApiService;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CRMController extends Controller
{
    protected $service;

    /**
     * ApiController constructor.
     * @param ApiService $service
     */
    public function __construct(ApiService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){
        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();
        return view('index', compact(['devices', 'brands', 'lines', 'source', 'city', 'latitude', 'longitude']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index2(Request $request){
        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();
        return view('index2', compact(['devices', 'brands', 'lines', 'source', 'city', 'latitude', 'longitude']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index3(Request $request){
        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();
        return view('tests.index3', compact(['devices', 'brands', 'lines', 'source', 'city', 'latitude', 'longitude']));
    }

    //current monthly phone bill amount and the number of lines
    public function index4(Request $request){
        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();
        return view('tests.index4', compact(['devices', 'brands', 'lines', 'source', 'city', 'latitude', 'longitude']));
    }

    public function index5(Request $request){
        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();
        return view('tests.index5', compact(['devices', 'brands', 'lines', 'source', 'city', 'latitude', 'longitude']));
    }

    public function script(Request $request){
        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();
        return view('tests.script', compact(['devices', 'brands', 'lines', 'source', 'city', 'latitude', 'longitude']));
    }

    public function scriptTest(Request $request){
        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();

        $top_title_1 = SeanSettings::SCRIPT_TEST__TOP_TITLE_1;
        $top_title_2 = SeanSettings::SCRIPT_TEST__TOP_TITLE_2;
        $placeholder_1 = SeanSettings::SCRIPT_TEST__FORM_TOP__PLACEHOLDER_1;
        $placeholder_2 = SeanSettings::SCRIPT_TEST__FORM_TOP__PLACEHOLDER_2;
        $form_title = SeanSettings::SCRIPT_TEST__FORM_TITLE;
        $button_text = SeanSettings::SCRIPT_TEST__FORM_BUTTON_TEXT;

        return view('tests.script-test', compact(
            ['devices', 'brands', 'lines', 'source', 'city', 'latitude', 'longitude',
                'placeholder_1', 'placeholder_2', 'form_title', 'top_title_1', 'top_title_2', 'button_text'
            ]));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loading(Request $request){
        if (!session('carrier') && !env('APP_DEBUG'))
            return redirect()->to('/');

        $info = $this->service->getGeoInfoByIp($request->ip());
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        return view('loading', compact(['latitude', 'longitude']));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article(Request $request){
        $source = $request->utm_source;
        return view('article', compact('source'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function leadCreate(Request $request){
        $data = $request->all();
        $data['ip'] = $request->ip();
        $brand = $this->service->getBrandImages($data['carrier'] ?? '');
        $response = $this->service->leadCreate($data);

        if(!empty($brand) && !$response['error']){
            return redirect()->to('loading')->with([
                'url_image_1'=> $brand['image1'] ?? '',
                'url_image_2'=> $brand['image2'] ?? '',
                'carrier' => $data['carrier'] ?? ''
            ]);
        }

        return redirect()->to('/');
    }

    /**
     * @param LeadCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function leadCreate2(LeadCreateRequest $request){
        $data = $request->all();
        session(['session_phone' => $data['phone'] ?? '']);
        $carrier = $data['carrier'];
        $current_bill = $data['current_bill'];
        $data['ip'] = $request->ip();
        $brand = $this->service->getBrandImages($data['carrier']);
        $response = $this->service->leadCreate($data);

        if(!empty($brand) && !$response['error']){
            return redirect()->to('loading')->with([
                'url_image_1'=> $brand['image1'] ?? '',
                'url_image_2'=> $brand['image2'] ?? '',
                'carrier' => $data['carrier'] ?? ''
            ]);
        }

        return redirect()->to('/');
    }


    public function leadCreate3(LeadCreateRequest $request){
//        $data = $request->all();
//        session(['session_phone' => $data['phone'] ?? '']);
//        $carrier = $data['carrier'];
//        $current_bill = $data['current_bill'];
//        $data['ip'] = $request->ip();
//        $brand = $this->service->getBrandImages($data['carrier'] ?? '');
//        $response = $this->service->leadCreate($data);


            return redirect()->to('loading')->with([
                'url_image_1'=> $brand['image1'] ?? '',
                'url_image_2'=> $brand['image2'] ?? '',
                'carrier' => $data['carrier'] ?? ''
            ]);


    }

    public function leadCreateScriptTest(Request $request){
//        $data = $request->all();
//        session(['session_phone' => $data['phone'] ?? '']);
//        $carrier = $data['carrier'];
//        $current_bill = $data['current_bill'];
//        $data['ip'] = $request->ip();
//        $brand = $this->service->getBrandImages($data['carrier'] ?? '');
//        $response = $this->service->leadCreate($data);

        return redirect()->to('script-test-loading')->with([
            'url_image_1'=> $brand['image1'] ?? '',
            'url_image_2'=> $brand['image2'] ?? '',
            'carrier' => $data['carrier'] ?? ''
        ]);
    }

    public function scriptTestLoading(Request $request){
//        if (!session('carrier') && !env('APP_DEBUG'))
//            return redirect()->to('/');

        $info = $this->service->getGeoInfoByIp($request->ip());
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        return view('tests.script-test-loading', compact(['latitude', 'longitude']));
    }

    public function scriptTestResults(Request $request){
//        if (!session('carrier') && !env('APP_DEBUG'))
//            return redirect()->to('/');

        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();

        $info = $this->service->getGeoInfoByIp($request->ip());
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        return view('tests.script-test-results', compact([
            'latitude', 'longitude', 'city', 'source', 'brands', 'devices', 'lines', 'info'
        ]));
    }

    // when a new multi-step-form appeared at results page
    public function leadCreateScriptTestAfterResults(Request $request){
//        if (!session('carrier') && !env('APP_DEBUG'))
//            return redirect()->to('/');

        $info = $this->service->getGeoInfoByIp($request->ip());
        $city = $info['city'];
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        $source = $request->utm_source;
        $brands = $this->service->getBrands();
        $devices = $this->service->getDevices();
        $lines = $this->service->getLines();

        $info = $this->service->getGeoInfoByIp($request->ip());
        $latitude = $info['latitude'];
        $longitude = $info['longitude'];
        return view('tests.lead-create-scripts-after-results', compact([
            'latitude', 'longitude', 'city', 'source', 'brands', 'devices', 'lines', 'info'
        ]));
    }

    /**
     * Get Banner TestSettings Data
     *
     * @return array
     */
    public function getBannerContent(){
        $filePath = public_path('/files/settings.json');
        $responseData = ['error' => true, 'data' => [], 'message' => ''];
        if(file_exists($filePath)){
            $banners = json_decode(file_get_contents($filePath), true);
            if(json_last_error() === JSON_ERROR_NONE){
                $responseData['error'] = false;
                foreach ($banners as $key => $banner){
                    $images = $this->service->getBrandImages($banner['carrier']);
                    $banners[$key]['image'] = !empty($images) ? $images['image1'] : '';
                }
                $responseData['data'] = $banners;
            }else{
                $error = json_last_error_msg();
                $responseData['message'] = "Not valid JSON string ($error)";
            }
        }else{
            $responseData['message'] = "File $filePath not found";
        }
        return $responseData;
    }

    /**
     * @param LeadCreateRequest $request
     * @return JsonResponse
     */
    public function leadCheck(LeadCreateRequest $request) {
        return new JsonResponse([], 200);
    }
}
