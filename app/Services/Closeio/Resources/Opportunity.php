<?php

namespace App\Services\Closeio\Resources;

use App\Services\Closeio\CloseioClient;

class Opportunity extends CloseioClient
{
    
    const ENDPOINT_OPPORTUNITY = 'opportunity';

    /**
     *
     * @return void
     */
    public function __construct()
    {
        $this->endpoint = self::ENDPOINT_OPPORTUNITY;
    }

}
