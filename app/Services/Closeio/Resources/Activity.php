<?php

namespace App\Services\Closeio\Resources;

use App\Services\Closeio\CloseioClient;

class Activity extends CloseioClient
{
    
    const ENDPOINT_ACTIVITY = 'activity';

    /**
     * Activity constructor.
     */
    public function __construct()
    {
        $this->endpoint = self::ENDPOINT_ACTIVITY;
    }

}
