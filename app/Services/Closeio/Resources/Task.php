<?php

namespace App\Services\Closeio\Resources;

use App\Services\Closeio\CloseioClient;

class Task extends CloseioClient
{
    
    const ENDPOINT_TASK = 'task';

    /**
     *
     * @return void
     */
    public function __construct()
    {
        $this->endpoint = self::ENDPOINT_TASK;
    }

}
