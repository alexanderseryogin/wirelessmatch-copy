<?php

namespace App\Services\Closeio\Resources;

use App\Services\Closeio\CloseioClient;

class Contact extends CloseioClient
{
    
    const ENDPOINT_CONTACT = 'contact';

    /**
     *
     * @return void
     */
    public function __construct()
    {
        $this->endpoint = self::ENDPOINT_CONTACT;
    }

}
