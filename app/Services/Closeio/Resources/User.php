<?php

namespace App\Services\Closeio\Resources;

use App\Services\Closeio\CloseioClient;

class User extends CloseioClient
{
    
    const ENDPOINT_USER = 'user/{id}';

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->endpoint = self::ENDPOINT_USER;
    }

}
