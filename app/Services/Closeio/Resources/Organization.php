<?php

namespace App\Services\Closeio\Resources;

use App\Services\Closeio\CloseioClient;

class Organization extends CloseioClient
{
    
    const ENDPOINT_ORGANIZATION = 'organization';

    /**
     *
     * @return void
     */
    public function __construct()
    {
        $this->endpoint = self::ENDPOINT_ORGANIZATION;
    }

}
