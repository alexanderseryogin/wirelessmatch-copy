<?php

namespace App\Services\Closeio;

use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Log;


class CloseioClient
{
    protected $endpoint;

    /**
     * CloseioClient constructor.
     */
    public function __construct()
    {
        $this->currentCarrierFieldId = env('LINES_FIELD_ID', '');
        $this->linesFieldId = env('LINES_CURRENT_CARRIER_ID', '');

        $this->lead = new Resources\Lead();
        $this->contact = new Resources\Contact();
        $this->activity = new Resources\Activity();
        $this->opportunity = new Resources\Opportunity();
        $this->task = new Resources\Task();
        $this->user = new Resources\User();
        $this->organization = new Resources\Organization();
    }

    /**
     * @param string $method
     * @param $endpoint
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function call($method = 'POST', $endpoint, $data = [])
    {
        $g_client = new GuzzleClient([
            'base_uri' => env('API_HOST', '') . '/',
            'auth' => [
                env('API_KEY', ''),
                '',
            ],
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
        $dataHttpQ = http_build_query($data);
        $endpoint = $method == 'GET' && $dataHttpQ ? "{$endpoint}/?{$dataHttpQ}" : "{$endpoint}/";
        $data = $data ? ['json' => (array) $data] : [];

        Log::info('Closeio api request: ', ['method' => $method, 'endpoint' => $endpoint, 'data' => $data]);

        $responseData = [];
        $error = false;

        try {
            $response = $g_client->request($method, $endpoint, $data);
            $responseData = json_decode($response->getBody(), true);
            Log::info('Closeio api response: ', ['body' => $responseData]);

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            Log::error('Closeio api request error: ', ['error' => $e]);
            $error = $e->getResponse() ? json_decode($e->getResponse()->getBody()->getContents()) : true;
        } catch (\Exception $e) {
            Log::error('Closeio api error: ', ['error' => $e]);
            $error = true;
        }

        return ['response' => $responseData, 'error' => $error];
    }

    /**
     * Retrieve a single resource
     *
     * @param $id
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($id, $data = [])
    {
        $endpoint = $this->endpoint . '/' . $id;
        return $this->call('GET', $endpoint, $data);
    }

    /**
     * List or search for resource(s)
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search()
    {
        return $this->call('GET', $this->endpoint);
    }

    /**
     * Delete a resource
     *
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete($id)
    {
        $endpoint = $this->endpoint . '/' . $id;
        return $this->call('DELETE', $endpoint);
    }

    /**
     * Update an existing resource
     *
     * @param $id
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($id, $data)
    {
        $endpoint = $this->endpoint . '/' . $id;
        return $this->call('PUT', $endpoint, $data);
    }

    /**
     * Create a new resource
     *
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($data)
    {
        $endpoint = $this->endpoint;
        return $this->call('POST', $endpoint, $data);
    }

}
