<?php

namespace App\Services;


use App\Services\Closeio\CloseioClient;

class ApiService
{
    /**
     * @var CloseioClient
     */
    private $client;

    private $isTestForm;

    /**
     * ApiService constructor.
     * @param CloseioClient $client
     */
    public function __construct(CloseioClient $client){
        $this->client = $client;
        $this->isTestForm = env('APP_DEBUG', false);
    }

    /**
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function leadCreate($data){

//        $devices = $this->getDevices();
//        $arrStrDevices = [];
//        foreach ($data['devices'] as $key => $value){
//            if($value != 0)
//                $arrStrDevices[] = ($devices[$key]['name'] . '(' . $value . ')');
//        }

        $params = [
            'url' =>  env('SOURCE_URL'),

            'name' => $data['name'] ?? '',

            'status_id' => env('STATUS_ID'),
            'contacts' => [
                [
                    'name' => $data['name'] ?? '',
                    'phones' => [
                        [
                            'type' => 'office',
                            'phone' => $data['phone'] ?? ''
                        ]
                    ]
                ]
            ]
        ];

        if(isset($data['current_bill']))
            $params['custom.' . env('CUSTOM_FIELD_LEAD_CURRENT_BILL')] = $data['current_bill'];

        if(isset($data['zip_code']))
            $params['custom.' . env('CUSTOM_FIELD_LEAD_ZIP_CODE')] = $data['zip_code'];

        if(isset($data['address']))
            $params['custom.' . env('CUSTOM_FIELD_LEAD_ADDRESS')] = $data['address'];

        if(isset($data['source']))
            $params['custom.' . env('CUSTOM_FIELD_LEAD_SOURCE_ID')] = $data['source'];


        $params['custom.' . env('CUSTOM_FIELD_IP_ID')] = $data['ip'] ?? '';
        $params['custom.' . env('CUSTOM_FIELD_CURRENT_CARRIER_ID')] = $data['carrier'] ?? '';
        $params['custom.' . env('CUSTOM_FIELD_LINES_ID')] = $data['lines'] ?? '';

//        $params['custom.' . env('CUSTOM_FIELD_CURRENT_BILL_ID')] = $data['pay'];
//        $params['custom.' . env('CUSTOM_FIELD_DATA_ID')] = $data['unlimited_data'];
//        $params['custom.' . env('CUSTOM_FIELD_TENURE_ID')] = $data['tenure'];
//        $params['custom.' . env('CUSTOM_FIELD_DEVICES_ID')] = implode(", ", $arrStrDevices);

        if($this->isTestForm){
            $params['error'] = false;
            return $params;
        }else{
            return $this->client->lead->create($params);
        }
    }

    /**
     * @param $carrier
     * @return array
     */
    public function getBrandImages($carrier){
        switch ($carrier){
            case 'Cricket': return ['image1' => 'images/loading-2-ico-cricket.png', 'image2' => 'images/load-2-logo.png'];
            case 'MetroPCS': return ['image1' => 'images/loading-2-ico-metroPCS.png', 'image2' => 'images/load-2-logo.png'];
            case 'T-Mobile': return ['image1' => 'images/loading-2-ico-tmobile.png', 'image2' => 'images/load-2-logo.png'];
            case 'AT&T': return ['image1' => 'images/loading-2-ico-att.png', 'image2' => 'images/load-2-logo.png'];
            case 'Sprint': return ['image1' => 'images/loading-2-ico-sprint.png', 'image2' => 'images/load-2-logo.png'];
            case 'Verizon': return ['image1' => 'images/loading-2-ico-verizon.png', 'image2' => 'images/load-2-logo.png'];
        }
        return [];
    }

    /**
     * @return array
     */
    public function getDevices(){
        return [
            ['name' => 'Apple iPad', 'url' => 'images/phone-images/Apple-iPad.png'],
            ['name' => 'iPhone 6', 'url' => 'images/phone-images/Apple-iPhone-6.jpg'],
            ['name' => 'iPhone 7', 'url' => 'images/phone-images/Apple-iPhone-7.jpg'],
            ['name' => 'iPhone 8', 'url' => 'images/phone-images/Apple-iPhone-8.jpg'],
            ['name' => 'iPhone X', 'url' => 'images/phone-images/Apple-iPhone-X.jpg'],
            ['name' => 'Other Phone', 'url' => 'images/phone-images/BYOD-Phone.png'],
            ['name' => 'Other Tablet', 'url' => 'images/phone-images/BYOD-Tablet.png'],
            ['name' => 'LG Phones', 'url' => 'images/phone-images/LG-Phones.png'],
            ['name' => 'Motorola Phones', 'url' => 'images/phone-images/Motorola-Phones.png'],
            ['name' => 'Samsung Galaxy Note8', 'url' => 'images/phone-images/Samsung-Galaxy-Note8.png'],
            ['name' => 'Samsung Galaxy S7', 'url' => 'images/phone-images/Samsung-Galaxy-S7.jpg'],
            ['name' => 'Samsung Galaxy S8', 'url' => 'images/phone-images/Samsung-Galaxy-S8.jpg'],
            ['name' => 'Samsung Galaxy S9', 'url' => 'images/phone-images/Samsung-Galaxy-S9.jpg']
        ];
    }

    /**
     * @return array
     */
    public function getBrands(){
        return ['Cricket', 'MetroPCS', 'T-Mobile', 'AT&T', 'Sprint', 'Verizon'];
    }

    /**
     * @param $source
     * @return string
     */
    public function getSource($source){
        switch ($source) {
            case 'customer_referral': return 'Customer Referral';
            case 'facebook': return 'Facebook';
            case 'instagram': return 'Instagram';
            case 'rep_referral': return 'Rep Referral';
            default:  return '';
        }
    }

    /**
     * @return array
     */
    public function getLines(){
        $lines = [];
        $countItems = 10;
        for ($i = 1; $i < $countItems + 1; $i++)
            $lines[$i] = $countItems != $i ? $i : $i.'+';
        return $lines;
    }

    /**
     * @param $ip
     * @return array
     */
    public function getGeoInfoByIp($ip){
        try{
            $location = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
            if($location['geoplugin_latitude'] && $location['geoplugin_longitude'] && $location['geoplugin_city']){
                return [
                    'latitude' => $location['geoplugin_latitude'],
                    'longitude' => $location['geoplugin_longitude'],
                    'city' => $location['geoplugin_city']
                ];
            }
        }catch (\Exception $exception){}

        try{
            $location = unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
            if($location['lat'] && $location['lon'] && $location['city']){
                return [
                    'latitude' => $location['lat'],
                    'longitude' => $location['lon'],
                    'city' => $location['city']
                ];
            }
        }catch (\Exception $exception){}

        return ['latitude' => -34.397, 'longitude'=> 150.644, 'city' => 'Orlando'];
    }
}