<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CRMController@index');
Route::get('/2', 'CRMController@index2');

/* -- tests --*/
Route::get('/3', 'CRMController@index3');
Route::get('/tests/3', 'CRMController@index3');
Route::get('/4', 'CRMController@index4');
Route::get('/tests/4', 'CRMController@index4'); //current monthly phone bill amount and the number of lines
Route::get('/5', 'CRMController@index5');
Route::get('/tests/5', 'CRMController@index5'); //current monthly phone bill amount and the number of
Route::get('/script', 'CRMController@script');
Route::get('/script-test', 'CRMController@scriptTest');
Route::get('/script-test-loading', 'CRMController@scriptTestLoading');
Route::get('/script-test-results', 'CRMController@scriptTestResults');
/* -- end tests --*/

Route::get('/loading', 'CRMController@loading');
Route::get('/article', 'CRMController@article');

Route::get('/careers', function () {
    return view('careers');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/generic-blog', function () {
    return view('generic-blog');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::get('/results', function () {
    return view('results');
});

Route::get('/results2', function () {
    return view('results2');
});

Route::get('/results3', function () {
    return view('results3');
});

Route::get('/terms', function () {
    return view('terms');
});

Route::post('/lead-create', 'CRMController@leadCreate');
Route::post('/lead-check', 'CRMController@leadCheck');
Route::post('/lead-create2', 'CRMController@leadCreate2');
Route::post('/lead-create3', 'CRMController@leadCreate3');
Route::post('/lead-create-script-test', 'CRMController@leadCreateScriptTest');
Route::post('/lead-create-script-test-after-results', 'CRMController@leadCreateScriptTestAfterResults');

Route::get('/banner-content', 'CRMController@getBannerContent');

Route::get('/admin', 'Auth\LoginController@showLoginForm');
Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::post('/admin/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/admin/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/admin/dashboard/index2', 'DashboardController@index2');
    Route::get('/admin/dashboard/index3', 'DashboardController@index3');
    Route::get('/admin/dashboard/index4', 'DashboardController@index4');
    Route::get('/admin/dashboard/change-button-text', 'DashboardController@changeButtonText');
});

