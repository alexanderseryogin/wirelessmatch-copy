<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminButtonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_buttons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('active');

            $table->integer('page_id');
            $table->foreign('page_id')->references('id')->on('admin_pages');

            $table->string('text', 255)->nullable()->default(null);
            $table->string('color_text', 255)->nullable()->default(null);
            $table->string('color_button', 255)->nullable()->default(null);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_buttons');
    }
}
