<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 03.09.18
 * Time: 11:06
 */
?>

<h1>EditPage: /4</h1>
<hr style="border-color: lightgrey;">
<h2>Buttons</h2>

@foreach($admin_buttons as $admin_button)
    <input type="text"
           class="button-id-{{$admin_button['id']}}"
           value="{{ $admin_button['text'] }}"
           data-button_id="{{ $admin_button['id'] }}">
    <button type="button" data-button_id="{{ $admin_button['id'] }}">Change text</button>
    <br><br>
@endforeach

<script src="{{asset('js/app.js')}}" defer></script>
<script src="{{asset('js/admin.js')}}" defer></script>




