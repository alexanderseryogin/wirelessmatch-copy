@extends('layouts.app', ['menu' => 'dashboard'])

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Dashboard</h3></div>
                <div class="panel-body">
                    <a href="dashboard/index1">
                        <button class="btn btn-primary">/1</button>
                    </a>
                    <a href="dashboard/index2">
                        <button class="btn btn-primary">/2</button>
                    </a>
                    <a href="dashboard/index3">
                        <button class="btn btn-primary">/3</button>
                    </a>
                    <a href="dashboard/index4">
                        <button class="btn btn-primary">/4</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
