<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>FAQ</title>
    <link rel="icon" href="images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome-4.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>

<!--Banner Start-->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="banner-logo">
                    <a href="/"><img src="images/logo.png" alt="pic" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="banner-sidebar">
                    <div class="banner-call hidden-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i>8444WMATCH</a>
                    </div>
                    
                    <div class="banner-call visible-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                    </div>
                    
                    <div class="banner-hrader">
                        <div class="menu-main">
                            <a href="#menu" id="toggle"><span></span></a>

                            <div id="menu">
                              <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="job.html">Jobs/Careers</a></li>
                                  <li><a href="contact">Contact Us</a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="banner-main">
                    <div class="banner-header f-head jobs-head">
                        <h2><span>FAQ</span></h2>
                        <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!--Banner End-->




<!--FAQ Start-->
<section id="faq-res">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="all-header faq-header job-h">
                    <img src="images/header-line.png" alt="pic" class="img-responsive center-block">
                    <h3>Frequently Asked Questions</h3>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    Integer tempor dui vulputate, congue tortor et?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo">
                                    Praesent dapibus ipsum quis ligula tempor?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsetwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsethree">
                                    Nunc laoreet vestibulum ex, sed varius ligula?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsethree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
                                    Fusce lobortis purus vel eros euismod sagittis?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefive">
                                    Sed dapibus augue eget erat eleifend?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsesix">
                                    Ut dignissim, nibh vitae bibendum suscipit?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsesix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--FAQ End-->

<!--Footer Start-->
<section id="footer-other">
    <div class="container">
        @include('parts.footer-menu')
    </div>
    
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-bottom-text text-center">
                        <a href="#"><img src="images/social1.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social2.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social3.png" alt="pic" class="img-responsive"></a>
                        <p>&copy; 2018 Wireless Match. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer End-->

<!--JS link Start-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/slick.js"></script>
    <script type="text/javascript" src="js/venobox.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
        var theToggle = document.getElementById('toggle');

        // based on Todd Motto functions
        // https://toddmotto.com/labs/reusable-js/

        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }
        // addClass
        function addClass(elem, className) {
            if (!hasClass(elem, className)) {
                elem.className += ' ' + className;
            }
        }
        // removeClass
        function removeClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            }
        }
        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(" " + className + " ") >= 0 ) {
                    newClass = newClass.replace( " " + className + " " , " " );
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        theToggle.onclick = function() {
           toggleClass(this, 'on');
           return false;
        }
    </script>
<!--JS link End-->

</body>
</html>