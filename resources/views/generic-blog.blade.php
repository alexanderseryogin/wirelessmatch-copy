<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Generic</title>
    <link rel="icon" href="images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome-4.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/style.css">

    <?php if(file_exists(public_path('files/pixels/generic-blog-pixels.html'))) include public_path('files/pixels/generic-blog-pixels.html');?>
</head>

<body>

<!--Banner Start-->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="banner-logo">
                    <a href="/"><img src="images/logo.png" alt="pic" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="banner-sidebar">
                    <div class="banner-call hidden-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i>8444WMATCH</a>
                    </div>
                    
                    <div class="banner-call visible-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                    </div>
                    
                    <div class="banner-hrader">
                        <div class="menu-main">
                            <a href="#menu" id="toggle"><span></span></a>

                            <div id="menu">
                              <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="job.html">Jobs/Careers</a></li>
                                  <li><a href="contact">Contact Us</a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="banner-main">
                    <div class="banner-header f-head jobs-head">
                        <h2><span>Blog</span></h2>
                        <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!--Banner End-->




<!--Advertised Start-->
<section id="privacy">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="all-header advertised-header">
                    <img src="images/header-line.png" alt="pic" class="img-responsive hidden-xs">
                    <img src="images/header-line.png" alt="pic" class="img-responsive visible-xs">
                    <h4>Our</h4>
                    <h3>Lorem ipsum dolor</h3>
                </div>

                <div class="advertised-text generic-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eget enim pulvinar, facilisis lacus eget, feugiat ex. Duis suscipit congue auctor. Praesent mi enim, accumsan at ultrices id, tempus a justo. Cras eu tempor libero. Vivamus eu vestibulum nisl. Nullam iaculis accumsan egestas. Vivamus eget felis dapibus, semper purus finibus, ultrices quam.</p>
                    <p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sequi voluptas rem eos earum atque nulla assumenda sunt, expedita quo facilis corporis quod ut eum doloremque numquam ipsa, non optio. Optio iure quia unde, repellat natus, aliquam earum odit cum deserunt id quibusdam, illum explicabo consectetur suscipit. Corrupti voluptates hic quia, eum possimus velit laborum ipsam, perspiciatis, itaque asperiores odit saepe. Sit voluptatum sint non facilis ab? In id recusandae quibusdam voluptatum fuga, aperiam harum magnam eveniet suscipit, debitis quidem ad enim, perferendis quod maxime minus et est ipsa eos. Aperiam, distinctio quisquam architecto nostrum harum a temporibus incidunt voluptates.</p>
                    <p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, porro similique omnis nihil reiciendis sed tenetur corporis aut autem id natus, aliquid earum nam veritatis ut vel pariatur assumenda, consequuntur hic a sunt eaque. Nesciunt tempora harum aut eius modi, illo eos ducimus reiciendis, omnis itaque magni distinctio laudantium ipsa!</p>
                    
                    <p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, porro similique omnis nihil reiciendis sed tenetur corporis aut autem id natus, aliquid earum nam veritatis ut</p>
                    <div class="all-header advertised-header generic-hrader">
                        <h4>Ipsum dolor</h4>
                    </div>
                    <p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, porro similique omnis nihil reiciendis sed tenetur corporis aut autem id natus, aliquid earum nam veritatis ut vel pariatur assumenda, consequuntur hic a sunt eaque. Nesciunt tempora harum aut eius modi, illo eos ducimus reiciendis, omnis itaque magni distinctio laudantium ipsa!</p>
                    <p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, porro similique omnis nihil reiciendis sed tenetur corporis aut autem id natus, aliquid earum nam veritatis ut</p>
                    <p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam esse iure, perferendis minima temporibus facere? Saepe dolores eveniet delectus, placeat et repellat fugit accusamus, voluptatum excepturi eaque laudantium porro molestiae.</p>
                </div>
            </div>
            
            <div class="col-sm-4">
                <div class="advertised-text generic-box">
                    <div class="generic-list">
                        <h4>Latest News</h4>
                        <ul>
                            <li><p><b>Lorem ipsum dolor sit amet consectetur.</b></p>
                                <ul>
                                    <li><p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam esse iure. <a href="#">Read More...</a></p></li>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li><p><b>Lorem ipsum dolor sit amet consectetur.</b></p>
                                <ul>
                                    <li><p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam esse iure. <a href="#">Read More...</a></p></li>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li><p><b>Lorem ipsum dolor sit amet consectetur.</b></p>
                                <ul>
                                    <li><p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam esse iure. <a href="#">Read More...</a></p></li>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li><p><b>Lorem ipsum dolor sit amet consectetur.</b></p>
                                <ul>
                                    <li><p class="pt-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam esse iure. <a href="#">Read More...</a></p></li>
                                </ul>
                            </li>
                        </ul>
                    </div> 
                </div>
                
                <div class="advertised-text generic-box">
                    <div class="generic-list">
                        <h4>Site Pages</h4>
                        <ul>
                            <li><a href="/"><i class="fa fa-arrow-right" aria-hidden="true"></i> Home</a></li>
                            <li><a href="results"><i class="fa fa-arrow-right" aria-hidden="true"></i> Result</a></li>
                            <li><a href="images/"><i class="fa fa-arrow-right" aria-hidden="true"></i> Jobs/Careers</a>
                                <ul>
                                    <li><a href="job.html#aa"><i class="fa fa-arrow-right" aria-hidden="true"></i> Developer</a></li>
                                    <li><a href="job.html#bb"><i class="fa fa-arrow-right" aria-hidden="true"></i> Designer UI - UX</a></li>
                                    <li><a href="job.html#cc"><i class="fa fa-arrow-right" aria-hidden="true"></i> Full Stack Designer</a></li>
                                </ul>
                            </li>
                            <li><a href="privacy"><i class="fa fa-arrow-right" aria-hidden="true"></i> Privacy</a></li>
                            <li><a href="terms"><i class="fa fa-arrow-right" aria-hidden="true"></i> Terms</a></li>
                            <li><a href="faq"><i class="fa fa-arrow-right" aria-hidden="true"></i> Faq</a></li>
                            <li><a href="contact"><i class="fa fa-arrow-right" aria-hidden="true"></i> Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Advertised End-->

<!--Footer Start-->
<section id="footer-other">
    <div class="container">
        @include('parts.footer-menu')
    </div>
    
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-bottom-text text-center">
                        <a href="#"><img src="images/social1.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social2.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social3.png" alt="pic" class="img-responsive"></a>
                        <p>&copy; 2018 Wireless Match. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer End-->

<!--JS link Start-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/slick.js"></script>
    <script type="text/javascript" src="js/venobox.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
        var theToggle = document.getElementById('toggle');

        // based on Todd Motto functions
        // https://toddmotto.com/labs/reusable-js/

        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }
        // addClass
        function addClass(elem, className) {
            if (!hasClass(elem, className)) {
                elem.className += ' ' + className;
            }
        }
        // removeClass
        function removeClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            }
        }
        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(" " + className + " ") >= 0 ) {
                    newClass = newClass.replace( " " + className + " " , " " );
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        theToggle.onclick = function() {
           toggleClass(this, 'on');
           return false;
        }
    </script>
<!--JS link End-->

</body>
</html>