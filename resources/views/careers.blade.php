<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Jobs/Careers</title>
    <link rel="icon" href="images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome-4.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/style.css">


</head>

<body>

<!--Banner Start-->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="banner-logo">
                    <a href="/"><img src="images/logo.png" alt="pic" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="banner-sidebar">
                    <div class="banner-call hidden-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i>8444WMATCH</a>
                    </div>
                    
                    <div class="banner-call visible-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                    </div>
                    
                    <div class="banner-hrader">
                        <div class="menu-main">
                            <a href="#menu" id="toggle"><span></span></a>

                            <div id="menu">
                              <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="job.html">Jobs/Careers</a></li>
                                  <li><a href="contact">Contact Us</a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="banner-main">
                    <div class="banner-header jobs-head">
                        <h2>Careers</h2>
<h4>Available positions at Wireless Match.</h4>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!--Banner End-->




<!--FAQ Start-->
<script type="text/javascript" src="//app.jazz.co/widgets/basic/create/wirelessmatch" charset="utf-8"></script>
        
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div id="aa" class="panel-heading panal-head-pad">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    Developer <br><span>(Frontend & Backup end)</span>
                                </a>
                                <span class="date">June 20, 2018</span>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body job-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="cc" class="panel-heading panal-head-pad">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo">
                                    Full-Stack Designer
                                </a>
                                <span class="date">June 19, 2018</span>
                            </h4>
                        </div>
                        <div id="collapsetwo" class="panel-collapse collapse">
                            <div class="panel-body job-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="bb" class="panel-heading panal-head-pad">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsethree">
                                    UI / UX Designer
                                </a>
                                <span class="date">June 18, 2018</span>
                            </h4>
                        </div>
                        <div id="collapsethree" class="panel-collapse collapse">
                            <div class="panel-body ux-body job-body">
                                <p>Nulla sed purus quis tortor maximus aliquet eu quis nibh. Aenean <br>pellentesque auctor ante, sed semper mauris bibendum sed.</p>
                                
                                <p>Maecenas pellentesque nulla tortor. Donec tellus neque, pellentesque at <br>turpis non, porta commodo risus. Class aptent taciti sociosqu ad litora <br>torquent per conubia nostra, per inceptos himenaeos. Suspendisse id nibh <br>vel lacus pharetra eleifend. Curabitur pharetra mauris id magna luctus </p>
                                
                                <h6>Requiered Qualifications</h6>
                                
                                <ul>
                                    <li><p><i class="fa fa-arrow-right"></i>Nulla sed purus </p></li>
                                    <li><p><i class="fa fa-arrow-right"></i>Quis tortor maximus </p></li>
                                    <li><p><i class="fa fa-arrow-right"></i>Aliquet eu quis nibh </p></li>
                                    <li><p><i class="fa fa-arrow-right"></i>Aenean pellentesque</p></li>
                                </ul>
                                
                                <a href="#" class="btn">Apply</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading panal-head-pad">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
                                    Developer <br><span>(Frontend & Backup end)</span>
                                </a>
                                <span class="date">June 20, 2018</span>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse">
                            <div class="panel-body job-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading panal-head-pad">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefive">
                                    Full-Stack Developer
                                </a>
                                <span class="date">June 20, 2018</span>
                            </h4>
                        </div>
                        <div id="collapsefive" class="panel-collapse collapse">
                            <div class="panel-body job-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a accumsan purus. Curabitur imperdiet tellus nec volutpat consectetur. Fusce lobortis purus vel eros euismod sagittis. Vestibulum sit amet metus libero.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--FAQ End-->

<!--Footer Start-->
<section id="footer-other">
    <div class="container">
        <div class="row">
            @include('parts.footer-menu')
        </div>
    </div>
    
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-bottom-text text-center">
                        <a href="#"><img src="images/social1.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social2.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social3.png" alt="pic" class="img-responsive"></a>
                        <p>&copy; 2018 Wireless Match. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer End-->

<!--JS link Start-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/slick.js"></script>
    <script type="text/javascript" src="js/venobox.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
        var theToggle = document.getElementById('toggle');

        // based on Todd Motto functions
        // https://toddmotto.com/labs/reusable-js/

        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }
        // addClass
        function addClass(elem, className) {
            if (!hasClass(elem, className)) {
                elem.className += ' ' + className;
            }
        }
        // removeClass
        function removeClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            }
        }
        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(" " + className + " ") >= 0 ) {
                    newClass = newClass.replace( " " + className + " " , " " );
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        theToggle.onclick = function() {
           toggleClass(this, 'on');
           return false;
        }
    </script>
<!--JS link End-->

</body>
</html>