<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Results</title>
    <link rel="icon" href="images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome-4.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/venobox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/style.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120004445-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-120004445-1');
    </script>
		
</head>

<body>

<!--Banner Start-->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="banner-logo">
                    <a href="/"><img src="images/logo.png" alt="pic" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="banner-sidebar">
                    <div class="banner-call hidden-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i>8444WMATCH</a>
                    </div>
                    
                    <div class="banner-call visible-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                    </div>
                    
                    <div class="banner-hrader">
                        <div class="menu-main">
                            <a href="#menu" id="toggle"><span></span></a>

                            <div id="menu">
                              <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="job.html">Jobs/Careers</a></li>
                                  <li><a href="contact">Contact Us</a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="banner-main">
                    <div class="banner-header result-head">
                        <h2><span>Congratulations!</span></h2>
                        <h4>we've found you a wireless match and significant savings.</h4>
                        <p>One of our friendly Wireless Experts will be sending you a text <br>momentarily to discuss the saving that we've found for you.</p>
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="result-vid-wrap">
                    <div class="result-vid">
                        <iframe width="100%" height="320" src="https://www.youtube.com/embed/zPH8cZidSYQ"></iframe>
                    </div>
                    <div class="result-vid-bottom text-center">
                        <input type="text" placeholder="xxx-xxx-xxxx">
                        <a href="callto:8444WMATCH" class="btn">Call Me Now</a>
                        <p><i class="fa fa-lock" aria-hidden="true"></i> Your information is safe</p>
                        <img src="images/result-vid-ico.png" alt="" class="img-responsive center-block">
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
<!--Banner End-->
<!--vid-after start-->

<section id="vidafter">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-center">
                <div class="vidaf-wrap">
                    <img src="images/vid-aftr-01.png" alt="" class="img-responsive center-block">
                    <h5>25,000 customers</h5>
                </div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="vidaf-wrap">
                    <img src="images/vid-aftr-02.png" alt="" class="img-responsive center-block">
                    <h5>Were expanding to 15 additional cities in 2018</h5>
                </div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="vidaf-wrap">
                    <img src="images/vid-aftr-03.png" alt="" class="img-responsive center-block">
                    <h5>95% success rate</h5>
                </div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="vidaf-wrap">
                    <img src="images/vid-aftr-03.png" alt="" class="img-responsive center-block">
                    <h5>The entire process just takes 2 minutes</h5>
                </div>
            </div>
        </div>
    </div>
</section>

<!--vid-after End-->

@include('parts.faq')

<!--Footer Start-->
<section id="footer-other">
    <div class="container">
        <div class="row">
            @include('parts.footer-menu')
        </div>
    </div>
    
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-bottom-text text-center">
                        {{--<a href="#"><img src="images/social1.png" alt="pic" class="img-responsive"></a>--}}
                        <a href="#"><img src="images/social2.png" alt="pic" class="img-responsive"></a>
                        {{--<a href="#"><img src="images/social3.png" alt="pic" class="img-responsive"></a>--}}
                        <p>&copy; 2018 Wireless Match. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer End-->

<!--JS link Start-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/slick.js"></script>
    <script type="text/javascript" src="js/venobox.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
        var theToggle = document.getElementById('toggle');

        // based on Todd Motto functions
        // https://toddmotto.com/labs/reusable-js/

        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }
        // addClass
        function addClass(elem, className) {
            if (!hasClass(elem, className)) {
                elem.className += ' ' + className;
            }
        }
        // removeClass
        function removeClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            }
        }
        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(" " + className + " ") >= 0 ) {
                    newClass = newClass.replace( " " + className + " " , " " );
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        theToggle.onclick = function() {
           toggleClass(this, 'on');
           return false;
        }
    </script>
<!--JS link End-->

</body>
</html>