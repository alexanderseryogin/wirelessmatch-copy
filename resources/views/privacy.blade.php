<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Privacy Policy</title>
    <link rel="icon" href="images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome-4.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/style.css">

    <?php if(file_exists(public_path('files/pixels/privacy-pixels.html'))) include public_path('files/pixels/privacy-pixels.html');?>
</head>

<body>

<!--Banner Start-->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="banner-logo">
                    <a href="/"><img src="images/logo.png" alt="pic" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="banner-sidebar">
                    <div class="banner-call hidden-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i>8444WMATCH</a>
                    </div>
                    
                    <div class="banner-call visible-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                    </div>
                    
                    <div class="banner-hrader">
                        <div class="menu-main">
                            <a href="#menu" id="toggle"><span></span></a>

                            <div id="menu">
                              <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="job.html">Jobs/Careers</a></li>
                                  <li><a href="contact">Contact Us</a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="banner-main">
                    <div class="banner-header f-head jobs-head">
                        <h2>Privacy Policy</h2>
                        <h4>Below you'll find the Wireless Match privacy policy.</h4>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!--Banner End-->




<!--Advertised Start-->
<section id="privacy">
    <div class="container">
        
        
            <div class="row">
                <div class="col-sm-12">
                    <div class="all-header advertised-header">
                        <img src="images/header-line.png" alt="pic" class="img-responsive hidden-xs">
                        <img src="images/header-line.png" alt="pic" class="img-responsive visible-xs">
                        <h4>Our</h4>
                        <h3>Privacy Policy</h3>
                    </div>
                    
                    <div class="advertised-text">


					   <p>Wireless Match operates the www.wirelessmatch.com website, which provides the service of a free cellular phone bill analysis.</p>
<p>This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p>

					   <p>If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that we collect are used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p>

					   <p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at www.wirelessmatch.com , unless otherwise defined in this Privacy Policy.</p>

					   <p><strong>Information Collection and Use</strong></p>

					   <p>For a better experience while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to your name, phone number, and postal address. The information that we collect will be used to contact or identify you.</p>

					   <p><strong>Log Data</strong></p>

					   <p>We want to inform you that whenever you visit our Service, we collect information that your browser sends to us that is called Log Data. This Log Data may include information such as your computer’s Internet Protocol (“IP”) address, browser version, pages of our Service that you visit, the time and date of your visit, the time spent on those pages, and other statistics.</p>

					   <p><strong>Cookies</strong></p>

					   <p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your computer’s hard drive.</p>

					   <p>Our website uses these “cookies” to collection information and to improve our Service. You have the option to either accept or refuse these cookies, and know when a cookie is being sent to your computer. If you choose to refuse our cookies, you may not be able to use some portions of our Service.</p><p><strong>Service Providers</strong></p>

					   <p>We may employ third-party companies and individuals due to the following reasons:</p>

					   <ul><li class="first-child">To facilitate our Service;</li>

					   <li>To provide the Service on our behalf;</li>

					   <li>To perform Service-related services; or</li>

					   <li class="last-child">To assist us in analyzing how our Service is used.</li>

					   </ul><p>We want to inform our Service users that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>

					   <p><strong>Security</strong></p>

					   <p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>

					   <p><strong>Links to Other Sites</strong></p>

					   <p>Our Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites, <a href="http://1minutereview.org/infusionsoft-alternative-2" target="_blank">visit us</a>. We have no control over, and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>

					   <p><strong>Children’s Privacy</strong></p><p>Our Services do not address anyone under the age of 13. We do not knowingly collect personal identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>

					   <p><strong>Changes to This Privacy Policy</strong></p>

					   <p>We may update our Privacy Policy from time to time. Thus, we advise you to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page.</p>

					   <p><strong>Contact Us</strong></p>

					   <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.</p>

                        
                        
                    </div>
                </div>
            </div>
        
    </div>
</section>
<!--Advertised End-->

<!--Footer Start-->
<section id="footer-other">
    <div class="container">
        @include('parts.footer-menu')
    </div>
    
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-bottom-text text-center">
                        <a href="#"><img src="images/social1.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social2.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social3.png" alt="pic" class="img-responsive"></a>
                        <p>&copy; 2018 Wireless Match. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer End-->

<!--JS link Start-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/slick.js"></script>
    <script type="text/javascript" src="js/venobox.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
        var theToggle = document.getElementById('toggle');

        // based on Todd Motto functions
        // https://toddmotto.com/labs/reusable-js/

        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }
        // addClass
        function addClass(elem, className) {
            if (!hasClass(elem, className)) {
                elem.className += ' ' + className;
            }
        }
        // removeClass
        function removeClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            }
        }
        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(" " + className + " ") >= 0 ) {
                    newClass = newClass.replace( " " + className + " " , " " );
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        theToggle.onclick = function() {
           toggleClass(this, 'on');
           return false;
        }
    </script>
<!--JS link End-->

</body>
</html>