<div id="banner-box" class="banner-box">
    <div class="banner-content">
        <div class="banner-pic">
            <img src="images/banner-map.png" alt="pic" class="img-responsive">
        </div>
        <div class="banner-text">
            <h6>
                <span id="banner-city" class="banner-city">Altamonte Springs, FL</span>
                <br><span id="banner-save" class="banner-save">Rita saved $1,220</span>
            </h6>
            <img id="banner-image" src="" class="banner-image">
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function(){
            $.get('/banner-content', function (response) {
                if(!response.error && response.data.length){
                    let banners = response.data;
                    let currentKey = 0;
                    let banner = $('#banner-box');
                    let sec = 5;

                    showBanner();
                    setInterval(function() {
                        showBanner();
                    }, (sec * 2 * 1000));

                    function showBanner() {
                        $("#banner-city").text(banners[currentKey].city);
                        $("#banner-save").text(banners[currentKey].save);
                        $("#banner-image").attr("src", banners[currentKey].image);
                        banner.addClass('banner-box-visible');
                        setTimeout(function () {
                            banner.removeClass('banner-box-visible');
                            currentKey++;
                            if(currentKey === banners.length)
                                currentKey = 0;
                        }, sec * 1000);
                    }
                }else{
                    console.log('Banner error: ' + response.message);
                }
            });
        });
    </script>
@stop
