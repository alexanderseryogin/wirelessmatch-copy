<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.08.18
 * Time: 15:42
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 27.08.18
 * Time: 13:50
 */
?>

<!-- index2.form.top / 3 fields -->

<form id="multi-step-form" method="post" name="multi-step-form" action="{{url('lead-create')}}">
    {{ csrf_field() }}
    @if($source)
        <input type="hidden" name="source" value="{{$source}}">
    @endif
    <fieldset>
        <h3>Just 2 questions...!</h3>
        <input required type="text" name="name" placeholder="Full Name" class="form-control">
        <div class="phone-container">
            <input type="hidden" id="phone-number" name="phone">
            <input required type="tel" id="phone" class="custom-form-control" maxlength="12"> <!-- 10 digits + 2 sign "-" -->
        </div>
        <div class="help-error"></div>
        <div class="digits-left"></div>
        <button type="submit" class="next btn">Calculate Savings <i class="fa fa-arrow-right"></i></button>
        <div><img src="images/bs1.png" alt="pic" class="img-responsive"></div>
    </fieldset>

    <fieldset>
        <h3>Just 3 more questions…</h3>
        <select id="select-carrier" name="carrier" class="required select-frm field_wrapper form-control" data-style="">
            <option value="" hidden>Who is your current carrier?</option>
            @foreach($brands as $brand)
                <option value="{{$brand}}">{{$brand}}</option>
            @endforeach
        </select>
        <select id="select-line" name="lines" class="required select-frm field_wrapper form-control" data-style="">
            <option value="" hidden>How many lines do you have?</option>
            @foreach($lines as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
            @endforeach
        </select>
        <input type="text" name="zip_code" placeholder="What is your Zip Code?" class="input-margin-top required form-control">
        {{--<input type="text" id="current_bill" name="current_bill" placeholder="How much are you currently paying?" class="input-margin-top required form-control">--}}
        <div class="help-error"></div>
        {{--<input type="number" name="pay" placeholder="How much do you pay each month?" class="input-margin-top required form-control">--}}
        {{--<button type="submit" class="next btn">Calculate Savings <i class="fa fa-arrow-right"></i></button>--}}
        {{--<div><img src="images/bs1.png" alt="pic" class="img-responsive"></div>--}}

        <button id="send-form" type="submit" class="btn btn-sm" title="">Calculate Savings <i class="fa fa-arrow-right"></i></button>
        <div><img src="images/bs1.png" alt="pic" class="img-responsive"></div>
        <label class="info-block">
            By clicking continue and submitting this form, I affirm that I
            have read and agree to this websites Privacy Policy and Terms of Use. I am
            also providing express written consent to be contacted by Wireless Match
            or by one or more of Wireless Match’s partners at the phone number I have
            provided in this form, even if my phone number is listed on a Do Not Call Registry.
            I also agree that such contact may be made using an automatic telephone dialing and/or a
            SMS message, in which standard rates may apply. I can revoke my consent at any time.
            I also understand that my agreement to be contacted is not a condition of purchase, and I
            may call 1-844-WMATCH to speak with someone about lowering my wireless bill.
        </label>
    </fieldset>

    {{--<fieldset>--}}
    {{--<h3>Coverage Verification</h3>--}}
    {{--<div class="map-container">--}}
    {{--<label class="info-block">--}}
    {{--Please enter your address below so that our tool can determine tower locations and ensure 4G LTE coverage.--}}
    {{--</label>--}}
    {{--<input id="pac-input" type="text" name="address"--}}
    {{--placeholder="Please enter your address."--}}
    {{--class="required form-control">--}}
    {{--<div class="help-error"></div>--}}
    {{--<div id="map" class="google-map"></div>--}}
    {{--<div id="infowindow-content">--}}
    {{--<span id="place-name"  class="title"></span><br>--}}
    {{--<span id="place-address"></span>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<button id="send-form" type="submit" class="btn btn-sm" title="">Calculate Savings <i class="fa fa-arrow-right"></i></button>--}}
    {{--<div><img src="images/bs1.png" alt="pic" class="img-responsive"></div>--}}
    {{--<label class="info-block">--}}
    {{--By clicking continue and submitting this form, I affirm that I--}}
    {{--have read and agree to this websites Privacy Policy and Terms of Use. I am--}}
    {{--also providing express written consent to be contacted by Wireless Match--}}
    {{--or by one or more of Wireless Match’s partners at the phone number I have--}}
    {{--provided in this form, even if my phone number is listed on a Do Not Call Registry.--}}
    {{--I also agree that such contact may be made using an automatic telephone dialing and/or a--}}
    {{--SMS message, in which standard rates may apply. I can revoke my consent at any time.--}}
    {{--I also understand that my agreement to be contacted is not a condition of purchase, and I--}}
    {{--may call 1-844-WMATCH to speak with someone about lowering my wireless bill.--}}
    {{--</label>--}}
    {{--</fieldset>--}}


    {{--<fieldset>--}}
    {{--<h3 id="question-step-3">What are your devices?</h3>--}}
    {{--<div class="row">--}}
    {{--@foreach($devices as $key => $device)--}}
    {{--<div class="col-sm-3 col-md-3 col-xs-4 devices-container">--}}
    {{--<div class="img-container">--}}
    {{--<img src="{{$device['url']}}" alt="{{$device['url']}}">--}}
    {{--<div class="buttons-container">--}}
    {{--<button class="btn btn-plus">+</button>--}}
    {{--<input type="text" name="devices[]" readonly class="square" value="0">--}}
    {{--<button class="btn btn-minus">-</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<label>{{$device['name']}}</label>--}}
    {{--</div>--}}
    {{--@endforeach--}}

    {{--</div>--}}
    {{--<button type="submit" class="next btn">Calculate Savings <i class="fa fa-arrow-right"></i></button>--}}
    {{--<div><img src="images/bs1.png" alt="pic" class="img-responsive"></div>--}}
    {{--</fieldset>--}}

    {{--<fieldset>--}}
    {{--<input id="unlimited-data" type="hidden" name="unlimited_data">--}}
    {{--<h3>Almost done! Just 2 more questions…</h3>--}}
    {{--<div class="row form-step">--}}
    {{--<div class="col-lg-6">--}}
    {{--<div class="custom-field" name="unlimited_data">--}}
    {{--<label>Do you have unlimited data?</label>--}}
    {{--<div class="btn-group">--}}
    {{--<button type="button" class="btn-data btn">Yes</button>--}}
    {{--<button type="button" class="btn-data btn active">No</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-6">--}}
    {{--<input id="input-tenure" name="tenure" type="text" class="required form-control">--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<button id="send-form" type="submit" class="btn btn-sm" title="">Calculate Savings <i class="fa fa-arrow-right"></i></button>--}}

    {{--<div><img src="images/bs1.png" alt="pic" class="img-responsive"></div>--}}
    {{--<label style="color: #959798; margin-top: 13px;font-size: 13px;">By clicking continue and submitting this form, I affirm that I have read and agree to this websites Privacy Policy and Terms of Use. I am also providing express written consent to be contacted by Wireless Match or by one or more of Wireless Match’s partners at the phone number I have provided in this form, even if my phone number is listed on a Do Not Call Registry. I also agree that such contact may be made using an automatic telephone dialing and/or a SMS message, in which standard rates may apply. I can revoke my consent at any time. I also understand that my agreement to be contacted is not a condition of purchase, and I may call 1-844-WMATCH to speak with someone about lowering my wireless bill.</label>--}}
    {{--</fieldset>--}}
</form>

