<section id="faq">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="all-header faq-header">
                    <img src="images/header-line.png" alt="pic" class="img-responsive center-block">
                    <h3>FAQ</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseOne">
                                    How can you save me money?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>We're able to save you money by finding you promotions that you wouldn't be able to
                                    find on your own. We've spent years developing our savings tool which is able to
                                    scan through thousands of promotions across all major carriers. Our savings tool,
                                    combined with our knowledge of the wireless industry, allows us to find you
                                    substantial savings</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapsetwo">
                                    How long does this take?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsetwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Only seconds! Fill out the quick form above and one of our Wireless Experts will
                                    notify you of your savings.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapsethree">
                                    Does this cost me anything?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsethree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Nope! Wireless Match is a 100% FREE service.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapsefour">
                                    Is Wireless Match a carrier that provides service?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>No! Wireless Match is NOT carrier such as Verizon, AT&T, T-Mobile & Sprint. We DO NOT
                                    provide service. We simply find you ways to save on your current wireless bill.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapsefive">
                                    Can Wireless Match help me?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Chances are, YES! If you're currently using any of the major carriers, then we can
                                    help you save!'</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapsesix">
                                    What's the catch?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsesix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Absolutely none! Our mission is simple: To help people lower their wireless
                                    bills.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>