<div class="row">
    <div class="col-sm-4">
        <div class="footer-box">
            <h5>CONTACT US</h5>
            <p>Wireless Match<br>111 N. Orange Ave<br>Orlando, FL 32801</p>
            <a href="mailto:hello@wirelessmatch.com">hello@wirelessmatch.com</a>
            <span><a href="callto:8444WMATCH">8444WMATCH (Toll-Free)</a></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="footer-box">
            <h5>MENU</h5>
            <ul>
                <li><a href="privacy">Privacy Policy</a></li>
                <li><a href="terms">Terms & Conditions</a></li>
                <li><a href="contact">Contact Us</a></li>
            </ul>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="footer-box footer-box-none">
            <h5>CAREERS</h5>
            <ul>
                <li><a href="">Customer Experience Specialist</a></li>
                <li><a href="">Wireless Expert</a></li>
                <li><a href="">Customer Care Specialist</a></li>
            </ul>
            <a href="#" class="btn-f">Work With Us</a>
        </div>
    </div>
</div>

