<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Wirelessmatch-Results</title>
    <link rel="icon" href="images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome-4.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/venobox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/intlTelInput.css">
    <link rel="stylesheet" href="css/style2.css">

</head>

<body>

<!--Banner Start-->
<section id="banner" class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="banner-logo">
                    <a href="/"><img src="images/logo.png" alt="pic" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="banner-sidebar">
                    <div class="banner-call hidden-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                    </div>

                    <div class="banner-hrader">
                        <div class="menu-main">
                            <a href="#menu" id="toggle"><span></span></a>

                            <div id="menu">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="job.html">Jobs/Careers</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="banner-main">
                    <div class="banner-header result-head result-head-marbtm ">
                        <h2><span>Thank you!</span></h2>
                        <div class="saving-code-text text-center">
                            {{--<h3>We will call you at the time you specified</h3>--}}
                            {{--<a href="#" class="btn">FRANKM863</a>--}}
                        </div>
                        {{--<p>One of our friendly Wireless Experts will be sending you a text <br>momentarily. Please inform them of your savings code above.</p>--}}
                    </div>
                </div>
            </div>
        </div>

        <!--Result-adds Start-->
        {{--<section id="result-adds">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6 col-md-offset-1">--}}
                        {{--<div class="result-adds-header">--}}
                            {{--<img src="images/header-line.png" alt="pic" class="img-responsive hidden-sm hidden-xs">--}}
                            {{--<img src="images/header-line.png" alt="pic" class="img-responsive center-block visible-sm visible-xs">--}}
                            {{--<h3>ANNUAL&nbsp;SAVINGS:<span class="get_annual_savings"></span></h3>--}}
                            {{--<img src="images/purple_arrow.png" alt="pic" class="img-responsive purple_arrow hidden-xs hidden-sm">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4">--}}
                        {{--<div class="result-adds-box">--}}
                            {{--<h3>Cut Your <br>Cell Phone <br>Bill In Half </h3>--}}
                            {{--<img src="images/rainbow.png" alt="pic" class="img-responsive">--}}
                            {{--<img src="images/hand.png" alt="pic" class="img-responsive hand-img">--}}
                            {{--<a href="#" class="btn">Calculate Savings</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
        <!--Result-adds End-->



        {{-- <div class="paying-cont">
             <img src="images/white_arrow.png" alt="pic" class="img-responsive pay-arrow">
             <div class="row">
                 <div class="col-sm-5 col-sm-offset-1 col-xs-6">
                     <div class="paying">
                         <img src="images/man1.png" alt="pic" class="img-responsive man_pic">
                         <h3>What you’re paying now</h3>
                         <span>$350</span>
                     </div>
                 </div>
                 <div class="col-sm-5 col-xs-6">
                     <div class="paying">
                         <img src="images/man2.png" alt="pic" class="img-responsive man_pic man_pic1">
                         <h3>What you’ll pay after Wireless Match</h3>
                         <span>$150</span>
                     </div>
                 </div>
             </div>
         </div>--}}

    </div>
</section>
<!--Banner End-->



<!--Result-form Start-->
{{--<section id="result-form">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-6 col-sm-offset-3">--}}
                {{--<div class="result-form-main text-center">--}}
                    {{--<form method="post" action="">--}}
                        {{--<img src="images/header-line.png" alt="pic" class="img-responsive center-block">--}}
                        {{--<h3>Want&nbsp;to&nbsp;save<br>--}}
                            {{--$<span class="get_annual_savings"></span> right away?--}}
                        {{--</h3>--}}
                        {{--<input type="text" placeholder="XXX-XXX-XXX" class="form-control" value="{{session('session_phone')}}">--}}
                        {{--<button type="button" name="next_step" class="next btn call-me">Call Me Now</button>--}}
                    {{--</form>--}}
                    {{--@include('tests.parts.after_results_script_multi-step-form-top')--}}
                    {{--<img src="images/lock_info.png" alt="pic" class="img-responsive center-block lock-img">--}}
                    {{--<img src="images/bs1.png" alt="pic" class="img-responsive suc-logo">--}}
                    {{--<img src="images/bs2.png" alt="pic" class="img-responsive suc-logo">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}
<!--Result-form End-->

<!--email-add Start-->
{{--<section id="email-add">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-sm-12">--}}
{{--<div class="email-add-form">--}}
{{--<p>Earn <span>$10</span> for every friend that uses Wireless Match’s free <br>savings tool. Enter their email addresses below:</p>--}}
{{--<form action="">--}}
{{--<div class="input-single pl-0">--}}
{{--<input type="text" placeholder="1. Enter email address" class="form-control">--}}
{{--</div>--}}
{{--<div class="input-single">--}}
{{--<input type="text" placeholder="2. Enter email address" class="form-control">--}}
{{--</div>--}}
{{--<div class="input-single">--}}
{{--<input type="text" placeholder="3. Enter email address" class="form-control">--}}
{{--</div>--}}
{{--<div class="input-single">--}}
{{--<input type="text" placeholder="4. Enter email address" class="form-control">--}}
{{--</div>--}}
{{--<div class="input-single">--}}
{{--<input type="text" placeholder="5. Enter email address" class="form-control">--}}
{{--</div>--}}
{{--<button type="button" name="" class="btn">Submit</button>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}
<!--email-add End-->

<!--vid-after start-->
{{--<section id="vidafter">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="all-header">--}}
                {{--<img src="images/header-line.png" alt="pic" class="img-responsive center-block">--}}
                {{--<h3>What happens next?</h3>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row">--}}
            {{--<div class="work-main-slide vidaf-main-slide">--}}
                {{--<i class="fa fa-arrow-left  w-left  visible-xs"></i>--}}
                {{--<i class="fa fa-arrow-right w-right visible-xs"></i>--}}

                {{--<div class="vidaf-main">--}}
                    {{--<div class="col-md-3 col-sm-6 text-center">--}}
                        {{--<div class="vidaf-wrap">--}}
                            {{--<img src="images/vid-aftr-01.png" alt="" class="img-responsive center-block">--}}
                            {{--<h5>UTILIZE THE <br>SAVINGS TOOL</h5>--}}
                            {{--<p>I want a check box next to this one and checked and faded a bit to highlight that the user already did this.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-3 col-sm-6 text-center">--}}
                        {{--<div class="vidaf-wrap">--}}
                            {{--<img src="images/vid-aftr-02.png" alt="" class="img-responsive center-block">--}}
                            {{--<h5>PROVIDE <br>SAVINGS CODE</h5>--}}
                            {{--<p>In just a minute your Wireless Expert will send you a text message. Please provide your savings code to your Wireless Expert so that they are able to locate your savings.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-3 col-sm-6 text-center">--}}
                        {{--<div class="vidaf-wrap">--}}
                            {{--<img src="images/vid-aftr-03.png" alt="" class="img-responsive center-block">--}}
                            {{--<h5>ENABLE SAVINGS</h5>--}}
                            {{--<p class="para-top">Once your Wireless Expert receives the savings code that our tool has generated, they will proceed to locate your account and find your savings. At this time, your Wireless Expert may have a couple of additional questions for you.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-3 col-sm-6 text-center">--}}
                        {{--<div class="vidaf-wrap">--}}
                            {{--<img src="images/vid-aftr-03.png" alt="" class="img-responsive center-block">--}}
                            {{--<h5>SAVE</h5>--}}
                            {{--<p class="para-top">It’s time to save! After a couple of minutes, your saving on your monthly wireless bill. It’s that easy!</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
{{--</section>--}}
<!--vid-after End-->



<!--Footer Start-->
<section id="footer-other">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-sm-offset-1">
                <div class="footer-box">
                    <h5>CONTACT us</h5>

                    <p>Muncak hiking society <br>8605 Second Ave. <br>Silver Spring, MD 20910</p>
                    <a href="mailto:Info@wireless.com">Info@wireless.com</a>
                    <span><a href="callto:8444WMATCH">8444WMATCH (Toll-free)</a></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="footer-box">
                    <h5>About us</h5>

                    <ul>
                        <li><a href="#">Our Mission</a></li>
                        <li><a href="#">Our Team</a></li>
                        <li><a href="#">Media Center</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="footer-box">
                    <h5>Site page</h5>

                    <ul>
                        <li><a href="privacy.html">Privacy</a></li>
                        <li><a href="terms.html">Terms</a></li>
                        <li><a href="faq.html">FAQ</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="footer-box footer-box-none">
                    <h5>Jobs - Careers Page</h5>

                    <ul>
                        <li><a href="job.html#aa">Developer</a></li>
                        <li><a href="job.html#bb">Designer UI - UX</a></li>
                        <li><a href="job.html#cc">Full Stack Designer</a></li>
                    </ul>

                    <a href="#" class="btn-f">Work With Us</a>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-bottom-text text-center">
                        <a href="#"><img src="images/social1.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social2.png" alt="pic" class="img-responsive"></a>
                        <a href="#"><img src="images/social3.png" alt="pic" class="img-responsive"></a>
                        <p>&copy; 2018 Wireless Match. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer End-->

<!--JS link Start-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/slick.js"></script>
<script type="text/javascript" src="js/venobox.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/script-test-results.js"></script>
<script src="/build/js/intlTelInput.js"></script>
{{--<script src="js/mutli-step2.js"></script>--}}
<script>
    var theToggle = document.getElementById('toggle');

    // based on Todd Motto functions
    // https://toddmotto.com/labs/reusable-js/

    // hasClass
    function hasClass(elem, className) {
        return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
    }
    // addClass
    function addClass(elem, className) {
        if (!hasClass(elem, className)) {
            elem.className += ' ' + className;
        }
    }
    // removeClass
    function removeClass(elem, className) {
        var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
        if (hasClass(elem, className)) {
            while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
                newClass = newClass.replace(' ' + className + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }
    }
    // toggleClass
    function toggleClass(elem, className) {
        var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
        if (hasClass(elem, className)) {
            while (newClass.indexOf(" " + className + " ") >= 0 ) {
                newClass = newClass.replace( " " + className + " " , " " );
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        } else {
            elem.className += ' ' + className;
        }
    }

    theToggle.onclick = function() {
        toggleClass(this, 'on');
        return false;
    };

    let getAnnualSavings = localStorage.getItem('annualSavings');
    $('span.get_annual_savings').text(getAnnualSavings);

</script>
<!--JS link End-->

</body>
</html>