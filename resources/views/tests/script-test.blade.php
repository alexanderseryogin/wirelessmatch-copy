<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Lower Your Wireless Bill: Fast, Free, Secure | Wireless Match</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="Description"
          content="Make sure you're getting the best deal on your wireless plan at WirelessMatch.com.">
    <meta name="Keywords" content="Wireless Match, WirelessMatch.com, WirelessMatch">
    <link rel="icon" href="../images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/fontawesome-4.min.css">
    <link rel="stylesheet" href="../css/mutli-step.css">

    {{--<link rel="stylesheet" href="css/sprite.css">--}}
    {{--<link rel="stylesheet" href="css/sprite@2x.css">--}}
    <link rel="stylesheet" href="../css/intlTelInput.css">

    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/slick.css">

    @if (env('APP_ENV') == 'production')
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120004445-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-120004445-1');
        </script>

        <!-- Hotjar Tracking Code for www.wirelessmatch.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:914931,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
    @endif

</head>

<body>
@include('parts.static-banner')
<input id="latitude" type="hidden" value="{{$latitude}}">
<input id="longitude" type="hidden" value="{{$longitude}}">
<!--Banner Start-->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="banner-logo">
                    <a href="/"><img src="../images/wirelessmatch_logo.svg" alt="pic" class="img-responsive"></a>

                </div>
            </div>
            <div class="col-xs-6">
                <div class="banner-sidebar">
                    <div class="banner-call hidden-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i>8444WMATCH</a>
                    </div>

                    <div class="banner-call visible-xs">
                        <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                    </div>

                    <div class="banner-hrader">

                        <div class="menu-main">
                            <a href="#menu" id="toggle"><span></span></a>

                            <div id="menu">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="contact">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="banner-main">
                    <div class="banner-header">
                        <h3>{{ $top_title_1 }}</h3>
                        <h3>{{ $top_title_2 }}</h3>
                        <br>
                        <div class="banner-header-logo">
                            <a href="#"><img src="../images/cricket.svg" alt="pic" class="img-responsive lo-1"></a>
                            <a href="#"><img src="../images/MetroPCS_logo.svg" alt="pic" class="img-responsive lo-2"></a>
                            <a href="#"><img src="../images/T-Mobile_logo.svg" alt="pic"
                                             class="img-responsive ban-logo-mid lo-3"></a>
                            <a href="#"><img src="../images/AT&T_logo_2016.svg" alt="pic" class="img-responsive lo-4"></a>
                            <a href="#"><img src="../images/sprint-logo.svg" alt="pic" class="img-responsive lo-5"></a>
                            <a href="#"><img src="../images/verizon-logo.svg" alt="pic"
                                             class="img-responsive ban-logo-mid lo-3"></a>

                        </div>
                    </div>

                    <div class="banner-form-box">
                        <div class="banner-form">
                            @include('tests.parts.script-test_multi-step-form-top')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Banner End-->

<!--Advertised Start-->
<section id="advertised">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="all-header">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive center-block">
                    <h4>As advertised on</h4>
                </div>

                <div class="feature-logo">
                    <img src="../images/f1.png" alt="pic" class="img-responsive">
                    <img src="../images/f2.png" alt="pic" class="img-responsive">
                    <img src="../images/f3.png" alt="pic" class="img-responsive">
                    <img src="../images/f4.png" alt="pic" class="img-responsive">
                    <img src="../images/f5.png" alt="pic" class="img-responsive">
                    <img src="../images/f6.png" alt="pic" class="img-responsive feature-logo-big">
                </div>
            </div>
        </div>

        <div class="advertised-main">
            <div class="row">
                <div class="col-sm-6">
                    <div class="all-header advertised-header">
                        <img src="../images/header-line.png" alt="pic" class="img-responsive hidden-xs">
                        <img src="../images/header-line.png" alt="pic" class="img-responsive visible-xs">
                        <h4>What is</h4>
                        <h3>Wireless Match?</h3>
                    </div>

                    <div class="advertised-text">
                        <p>Wireless Match is a free tool & service that helps you save money on your monthly wireless
                            bill. In just seconds, our revolutionary savings tool and team of Wireless Experts, will get
                            to work and find you multiple ways for you to save! In fact, we're so successful in finding
                            deals, that the average Wireless Match user saves over $1,200 annually.</p>


                        <a href="#" class="btn hidden-xs">Lower My Bill!</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="advertised-pic text-center">
                        <img src="../images/advertised-pic.png" alt="pic" class="img-responsive center-block">
                        <a href="#" class="btn visible-xs">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Advertised End-->

<!--Savings Start-->
<section id="savings">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="savings-main">
                    <div class="savings-box">
                        <img src="../images/s1.png" alt="pic" class="img-responsive">
                        <h4>Fast</h4>
                    </div>
                    <div class="savings-box">
                        <img src="../images/s2.png" alt="pic" class="img-responsive">
                        <h4>Free</h4>
                    </div>
                    <div class="savings-box border-none">
                        <img src="../images/s3.png" alt="pic" class="img-responsive">
                        <h4>Secure</h4>
                    </div>
                    <div class="savings-box border-none">
                        <img src="../images/savings-arrow.png" alt="pic" class="img-responsive savings-last-pic hidden-xs">
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="savings-btn">
                    <a href="#" class="btn">Check My Savings</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Savings End-->

<!--Work Start-->
<section id="work">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="all-header">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive center-block">
                    <h3>How does it work?</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="work-main-slide">
                <i class="fa fa-arrow-left  w-left  visible-xs"></i>
                <i class="fa fa-arrow-right w-right visible-xs"></i>

                <div class="work-main">
                    <div class="col-sm-4">
                        <div class="work-box-main">
                            <div class="work-box">
                                <img src="../images/w1.png" alt="pic" class="img-responsive center-block">
                                <h5>Input Your Number</h5>
                                <p>The first step is to answer <br>a few quick questions.</p>
                            </div>
                            <img src="../images/work-boxbg.png" alt="pic"
                                 class="img-responsive work-boxbg-pic work-boxbg-left hidden-xs">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="work-box-main">
                            <div class="work-box">
                                <img src="../images/w2.png" alt="pic" class="img-responsive center-block">
                                <h5>We Review Your Plan</h5>
                                <p>Once we have your answers<br>we will take a look at<br> your current plan and
                                    devices.</p>
                            </div>
                            <img src="../images/work-boxbg.png" alt="pic"
                                 class="img-responsive work-boxbg-pic work-boxbg-top hidden-xs">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="work-box-main">
                            <div class="work-box">
                                <img src="../images/w3.png" alt="pic" class="img-responsive center-block">
                                <h5>We Find Your Savings</h5>
                                <p>In just minutes, we're able to<br>locate and notify you of<br>your potential savings!
                                </p>
                            </div>
                            <img src="../images/work-boxbg2.png" alt="pic" class="img-responsive work-boxbg-pic hidden-xs">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="work-btn text-center">
                    <a href="#" class="btn">Check My Savings</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Work End-->

<!--Help Start-->
<section id="help">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="../images/help-pic-mb-part2.png" alt="pic" class="img-responsive visible-xs center-block">
                <div class="all-header help-header">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive hidden-xs">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive center-block visible-xs">
                    <h4>We’ve helped {{$city}} residents</h4>
                    <h3>save MILLIONS!</h3>
                </div>
                <div class="all-header help-header">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive hidden-xs">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive center-block visible-xs">
                    <h4>18,000+ people in {{$city}} </h4>
                    <h3>have used <br>Wireless Match.</h3>
                </div>
                <div class="all-header help-header help-header-last">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive hidden-xs">
                    <img src="../images/header-line.png" alt="pic" class="img-responsive center-block visible-xs">
                    <h4>{{$city}} Residents Save</h4>
                    <h3>an Average of $1,200 annually.</h3>
                </div>
                <img src="../images/help-pic-mb-part1.png" alt="pic" class="img-responsive visible-xs center-block">
            </div>

            <div class="col-sm-6">
                <div class="help-img hidden-xs">
                    <img src="../images/help-pic.png" alt="pic" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
<!--Help End-->

<!--Convinced Start-->
<section id="convinced">
    <img src="../images/client-left.png" alt="pic" class="img-responsive convinced-left-pic">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="client-main-slide">
                        <div class="all-header convinced-header">
                            <img src="../images/header-line.png" alt="pic" class="img-responsive center-block">
                            <h4>Still not</h4>
                            <h3>Convinced?</h3>
                        </div>
                        <i class="fa fa-arrow-left  c-left"></i>
                        <i class="fa fa-arrow-right c-right"></i>

                        <div class="client-main">
                            <div class="col-sm-6">
                                <div class="client-box-main">
                                    <div class="client-box">
                                        <img src="../images/c1.png" alt="pic" class="img-responsive">
                                        <p>“I was surprised to find discounts so rapidly... which ended up saving me
                                            quite a bit from what I had been paying. Easy & fast - two thumbs up!”</p>
                                        <h5>Benrns Sturaro <br><span>Lake Buena Vista, FL</span></h5>
                                    </div>
                                    <img src="../images/work-boxbg.png" alt="pic" class="img-responsive client-boxbg-pic">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="client-box-main">
                                    <div class="client-box">
                                        <img src="../images/c2.png" alt="pic" class="img-responsive">
                                        <p>“The whole thing was so simple. I didn't think I'd wake up today and save
                                            $200 a month but I did. Thanks Wireless Match.”</p>
                                        <h5>Jaron<br><span>Clermont, FL</span></h5>
                                    </div>
                                    <img src="../images/work-boxbg.png" alt="pic"
                                         class="img-responsive client-boxbg-pic client-boxbg-top">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Convinced End-->

@include('parts.faq')

<!--Footer Start-->
<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="footer-header">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="banner-header">
                                <h3>CUT YOUR CELL </h3>
                                <h3 class="footer-hd">PHONE BILL IN HALF.</h3>
                                <br>
                                <div class="banner-header-logo">
                                    <a href="#"><img src="../images/cricket.svg" alt="pic" class="img-responsive lo-1"></a>
                                    <a href="#"><img src="../images/MetroPCS_logo.svg" alt="pic"
                                                     class="img-responsive lo-2"></a>
                                    <a href="#"><img src="../images/T-Mobile_logo.svg" alt="pic"
                                                     class="img-responsive ban-logo-mid lo-3"></a>
                                    <a href="#"><img src="../images/AT&T_logo_2016.svg" alt="pic"
                                                     class="img-responsive lo-4"></a>
                                    <a href="#"><img src="../images/sprint-logo.svg" alt="pic" class="img-responsive lo-5"></a>
                                    <a href="#"><img src="../images/verizon-logo.svg" alt="pic"
                                                     class="img-responsive ban-logo-mid lo-3"></a>
                                </div>

                                {{--@include('tests.parts.index4_multi-step-form-bottom')--}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('parts.footer-menu')
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-bottom-text text-center">

                        <a href="http://www.facebook.com/WirelessMatch"><img src="../images/social2.png" alt="pic" class="img-responsive"></a>

                        <p>&copy; 2018 Wireless Match. All Rights Reserved.</p>
                        <p>1 Use of Wireless Match (DBA WirelessMatch.com) is subject to our Terms and Privacy
                            Policy.</p>
                        <p>2 Wireless Match is not affiliated in any way with CBS, ABC, NBC News, CNN, FOX, & The Boston
                            Globe. Wireless Match paid advertisements may been seen on CBS, ABC, NBC News, CNN, FOX &
                            The Boston Globe's digital properties.</p>
                        <p>3 The specified use of this site and service is to accurately match consumers to the wireless
                            carrier that best meets their needs. We are not a wireless carrier, we do not provide
                            wireless service, and we are not directly affiliated with any particular wireless carrier.
                            We are not directly affiliated with Verizon Wireless, Sprint, AT&T, T-Mobile, Cricket, or
                            metroPCS. </p>
                        <p>4 All trademarks, logos, and service marks (collectively the "Trademarks") displayed are
                            registered and/or unregistered Trademarks of their respective owners.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer End-->

<!--JS link Start-->
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/slick.js"></script>
<script src="../build/js/intlTelInput.js"></script>
<script src="../js/script-test.js"></script><!-- TEST -->
<script src="../js/custom.js"></script>

{{--<script src="js/map-verification.js"></script>--}}
{{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA37i3cfQYjbzocNv-AmME8WKZ26UZQjUA&libraries=places&callback=initialize"></script>--}}

<script>


    var theToggle = document.getElementById('toggle');

    // based on Todd Motto functions
    // https://toddmotto.com/labs/reusable-js/

    // hasClass
    function hasClass(elem, className) {
        return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
    }

    // addClass
    function addClass(elem, className) {
        if (!hasClass(elem, className)) {
            elem.className += ' ' + className;
        }
    }

    // removeClass
    function removeClass(elem, className) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        if (hasClass(elem, className)) {
            while (newClass.indexOf(' ' + className + ' ') >= 0) {
                newClass = newClass.replace(' ' + className + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }
    }

    // toggleClass
    function toggleClass(elem, className) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, " ") + ' ';
        if (hasClass(elem, className)) {
            while (newClass.indexOf(" " + className + " ") >= 0) {
                newClass = newClass.replace(" " + className + " ", " ");
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        } else {
            elem.className += ' ' + className;
        }
    }

    theToggle.onclick = function () {
        toggleClass(this, 'on');
        return false;
    };

    console.log('script-test.blade.php OK');
</script>
<!--JS link End-->

@yield('scripts')
</body>

</html>