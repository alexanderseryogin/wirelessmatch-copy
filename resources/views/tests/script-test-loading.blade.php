<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Loading</title>
    <link rel="icon" href="images/fev_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome-4.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/slick.css">


    <!-- Global site tag (gtag.js) - Google Ads: 800671506 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-800671506"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-800671506');
    </script>



    <!-- Event snippet for Wireless Match Lead conversion page -->
    <script>
        gtag('event', 'conversion', {'send_to': 'AW-800671506/BXYYCNjgrocBEJKO5f0C'});
    </script>


    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '197003794229000');
        fbq('track', 'PageView');
    </script>

    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=197003794229000&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <script>
        fbq('track', 'Lead');
    </script>


    <script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10060677'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>




    <!-- Reddit Conversion Pixel -->
    <script>
        var now=Date.now();var i=new Image();i.src="https://alb.reddit.com/snoo.gif?q=CAAHAAABAAoACQAAAB1pTYF3AA==&s=ycnZPiCSfA-cBnXLH9eLKwmyG_0ImXwpKd-zWiijxMw=&ts="+now;
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://alb.reddit.com/snoo.gif?q=CAAHAAABAAoACQAAAB1pTYF3AA==&s=ycnZPiCSfA-cBnXLH9eLKwmyG_0ImXwpKd-zWiijxMw="/>
    </noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Reddit Conversion Pixel -->

    <img src="http://trends.revcontent.com/conv.php?t=JM1qKR4MgB4g3DXvKOrFeJXjihrFF7FcLuCgXmhyROCAxaUNRxFO9uoyitVid3k4" />


</head>

<body id="loading">
@include('parts.static-banner')
<input id="latitude" type="hidden" value="{{$latitude}}">
<input id="longitude" type="hidden" value="{{$longitude}}">

<div class="loading-wrapper">
    <section id="load">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="banner-logo">
                        <a href="/"><img src="images/wirelessmatch_logo.svg" alt="pic" class="img-responsive"></a>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="banner-sidebar">
                        <div class="banner-call hidden-xs">
                            <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i>8444WMATCH</a>
                        </div>

                        <div class="banner-call visible-xs">
                            <a href="callto:8444WMATCH"><i class="fa fa-volume-control-phone"></i></a>
                        </div>

                        <div class="banner-hrader">
                            <div class="menu-main">
                                <a href="#menu" id="toggle"><span></span></a>

                                <div id="menu">
                                    <ul>
                                        <li><a href="/">Home</a></li>
                                        <li><a href="job.html">Careers</a></li>
                                        <li><a href="contact">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clr"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    <div class="load-main">
                        <div class="loading-1">
                            <div class="banner-main">
                                <div class="banner-header load-head">
                                    <h2>Detecting Current Carrier</h2>
                                </div>
                                <div class="loading-ico-main">
                                    <div class="load-ico main-ld-ico ico-load-1 text-center">
                                        <img src="images/cricket.svg" alt="pic" class="img-responsive lo-1"></a>
                                        <img src="images/MetroPCS_logo.svg" alt="pic" class="img-responsive lo-2"></a>
                                        <img src="images/T-Mobile_logo.svg" alt="pic" class="img-responsive ban-logo-mid lo-3"></a>
                                        <img src="images/AT&T_logo_2016.svg" alt="pic" class="img-responsive lo-4"></a>
                                        <img src="images/sprint-logo.svg" alt="pic" class="img-responsive lo-5"></a>
                                        <img src="images/verizon-logo.svg" alt="pic" class="img-responsive ban-logo-mid lo-3"></a>
                                    </div>

                                    <div class="load-ico main-ld-ico ico-load-2 text-center">
                                        <img src="images/cricket.svg" alt="pic" class="img-responsive lo-1"></a>
                                        <img src="images/MetroPCS_logo.svg" alt="pic" class="img-responsive lo-2"></a>
                                        <img src="images/T-Mobile_logo.svg" alt="pic" class="img-responsive ban-logo-mid lo-3"></a>
                                        <img src="images/AT&T_logo_2016.svg" alt="pic" class="img-responsive lo-4"></a>
                                        <img src="images/sprint-logo.svg" alt="pic" class="img-responsive lo-5"></a>
                                        <img src="images/verizon-logo.svg" alt="pic" class="img-responsive ban-logo-mid lo-3"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="preloader-wrap">
                                <div class="percentage" id="precent"></div>
                                <div class="loader">
                                    <div class="trackbar">
                                        <div class="loadbar"></div>
                                    </div>
                                    <div class="glow"></div>
                                </div>
                            </div>

                        </div>




                        <div class="loading-2">
                            <div class="banner-main">
                                <div class="banner-header load-head">
                                    <h2>Analyzing Current Carrier</h2>
                                </div>
                                <div class="load-ico no-hov text-center">
                                    <img src="{{session('url_image_1')}}" alt="" class="img-responsive">
                                </div>

                                <div class="load-map">
                                    <div class="load-2-txt text-left">
                                        <h4>{{session('carrier')}}</h4>

                                        <p>{{session('carrier')}} is a provider of premium entertainment, cell phones, wireless plans, home internet & more.</p>

                                        <a href="callto:505-998-3793">800-423-5553</a>

                                        <p><span><i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                            </span></p>
                                    </div>
                                    <div id="map" class="load-2-map">

                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>



                        <div class="loading-3">
                            <div class="banner-main">
                                <div class="banner-header load-head">
                                    <h2>Reviewing Current Plan</h2>
                                </div>
                                <div class="load-ico no-hov text-center">
                                    <img src="{{session('url_image_1')}}" alt="" class="img-responsive">
                                </div>

                                <div class="load-map">
                                    <div class="load-2-txt text-left">
                                        <h4>{{session('carrier')}}</h4>

                                        <p>{{session('carrier')}} is a provider of premium entertainment, cell phones, wireless plans, home internet & more.</p>

                                        <a href="callto:505-998-3793">800-423-5553</a>

                                        <p><span><i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                               <i class="fa fa-star" aria-hidden="true"></i>
                                            </span></p>
                                    </div>
                                    <div id="map2" class="load-2-map">

                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>



                        <div class="loading-4">
                            <div class="banner-main">
                                <div class="banner-header load-head">
                                    <h2>Scanning Available Promos</h2>
                                </div>
                                <div class="load-ico no-hov text-center">
                                    <img src="{{session('url_image_1')}}" alt="" class="img-responsive">
                                </div>

                                <div class="load-promo">
                                    <div class="promo-parts text-center">
                                        <h5>Plan Rate <br>
                                            <span>Discounts</span>
                                        </h5>

                                        <p>Searching / Found</p>
                                        <img src="images/load-loading.gif" alt="" class="img-responsive center-block">
                                    </div>
                                    <div class="promo-parts no-brm text-center">
                                        <h5>Device <br>
                                            <span>Promotions</span>
                                        </h5>

                                        <p>Searching / Found</p>
                                        <img src="images/load-loading.gif" alt="" class="img-responsive center-block">
                                    </div>
                                    <div class="promo-parts text-center">
                                        <h5>Location Based <br>
                                            <span>Deductions</span>
                                        </h5>

                                        <p>Searching / Found</p>
                                        <img src="images/load-loading.gif" alt="" class="img-responsive center-block">
                                    </div>
                                    <div class="promo-parts no-br text-center">
                                        <h5>Valuable Customer <br>
                                            <span>Credits</span>
                                        </h5>

                                        <p>Searching / Found</p>
                                        <img src="images/load-loading.gif" alt="" class="img-responsive center-block">
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>



                        <div class="loading-5">
                            <div class="banner-main">
                                <div class="banner-header load-head">
                                    <h2>We've Found <span>Savings</span></h2>
                                </div>
                                <div class="load-ico no-hov text-center">
                                    <img src="{{session('url_image_1')}}" alt="" class="img-responsive">
                                </div>

                                <div class="load-promo">
                                    <div class="promo-parts text-center">
                                        <h5>Plan Rate <br>
                                            <span>Discounts</span>
                                        </h5>

                                        <div class="load-price-wrap wrap-bl">
                                            <h4><sup></sup>Found<sub></sub></h4>
                                            <p>Verified Match</p>
                                        </div>
                                    </div>
                                    <div class="promo-parts text-center">
                                        <h5>Device <br>
                                            <span>Promotions</span>
                                        </h5>

                                        <div class="load-price-wrap wrap-bl">
                                            <h4><sup></sup>Found<sub></sub></h4>
                                            <p>Verified Match</p>
                                        </div>
                                    </div>
                                    <div class="promo-parts text-center">
                                        <h5>Location Based <br>
                                            <span>Deductions</span>
                                        </h5>

                                        <div class="load-price-wrap wrap-bl">
                                            <h4><sup></sup>Found<sub></sub></h4>
                                            <p>Verified Match</p>
                                        </div>
                                    </div>
                                    <div class="promo-parts no-br text-center">
                                        <h5>Valuable Customer <br>
                                            <span>Credits</span>
                                        </h5>

                                        <div class="load-price-wrap wrap-bl">
                                            <h4><sup></sup>Found<sub></sub></h4>
                                            <p>Verified Match</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner End-->

    <!--JS link Start-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/slick.js"></script>
    <script src="js/custom.js"></script>
    <script>
        var theToggle = document.getElementById('toggle');

        // based on Todd Motto functions
        // https://toddmotto.com/labs/reusable-js/

        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }
        // addClass
        function addClass(elem, className) {
            if (!hasClass(elem, className)) {
                elem.className += ' ' + className;
            }
        }
        // removeClass
        function removeClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            }
        }
        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(" " + className + " ") >= 0 ) {
                    newClass = newClass.replace( " " + className + " " , " " );
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        theToggle.onclick = function() {
            toggleClass(this, 'on');
            return false;
        }
    </script>

    <script>
        // Note: This example requires that you consent to location sharing when
        // prompted by your browser. If you see the error "The Geolocation service
        // failed.", it means you probably did not give permission for the browser to
        // locate you.
        var map, map2, infoWindow;
        var styl = [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dadada"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#c9c9c9"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            }
        ];

        function initialize() {
            initMap();
            initMap2();
        }

        let latitude = parseFloat($('#latitude').val());
        let longitude =  parseFloat($('#longitude').val());

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: latitude, lng: longitude},
                zoom: 9,
                styles: styl,
            });
        }
        function initMap2() {
            map2 = new google.maps.Map(document.getElementById('map2'), {
                center: {lat: latitude, lng: longitude},
                zoom: 9,
                styles: styl,
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA37i3cfQYjbzocNv-AmME8WKZ26UZQjUA&callback=initialize">
    </script>

    <script>
        var width = 100,
            perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page.
            EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
            time = parseInt((EstimatedTime/1000)%60)*100;

        // Loadbar Animation
        $(".loadbar").animate({
            width: width + "%"
        }, time);

        // Loadbar Glow Animation
        $(".glow").animate({
            width: width + "%"
        }, time);

        // Percentage Increment Animation
        var PercentageID = $("#precent"),
            start = 0,
            end = 100,
            durataion = time;
        animateValue(PercentageID, start, end, durataion);

        function animateValue(id, start, end, duration) {

            var range = end - start,
                current = start,
                increment = end > start? 1 : -1,
                stepTime = Math.abs(Math.floor(duration / range)),
                obj = $(id);

            var timer = setInterval(function() {
                current += increment;
                $(obj).text(current + "%");
                //obj.innerHTML = current;
                if (current == end) {
                    clearInterval(timer);
                }
            }, stepTime);
        }

        setTimeout(function () {
            console.log('script-test-loading.blade.php before window.location.href = script-test-results OK');
            window.location.href = '/script-test-results';
        }, 16000); // 16000


        // Fading Out Loadbar on Finised
        /*setTimeout(function(){
          $('.preloader-wrap').fadeOut(300);
        }, time);*/

    </script>





    <!--JS link End-->
</div>
@yield('scripts')
</body>
</html>