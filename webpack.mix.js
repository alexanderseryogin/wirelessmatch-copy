let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.copy('node_modules/intl-tel-input/build/img/flags.png','public/build/img/flags.png');
mix.copy('node_modules/intl-tel-input/build/img/flags@2x.png','public/build/img/flags@2x.png');
mix.copy('node_modules/intl-tel-input/build/js/utils.js', 'public/build/js/utils.js');
mix.copy('node_modules/intl-tel-input/build/js/intlTelInput.js', 'public/build/js/intlTelInput.js');


mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('node_modules/intl-tel-input/src/css/sprite.scss', 'public/css')
    .sass('node_modules/intl-tel-input/src/css/sprite@2x.scss', 'public/css')
    .sass('node_modules/intl-tel-input/src/css/intlTelInput.scss', 'public/css');
